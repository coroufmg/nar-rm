clear all;
close all;

%% Putting macsin and nar-rm_lib library in the MATLAB Path
if isempty(regexp(path,['macsin_lib' pathsep], 'once'))
    addpath([pwd, '/macsin_lib']);    
end
if isempty(regexp(path,['nar-rm_lib' pathsep], 'once'))
    addpath([pwd, '/nar-rm_lib']); 
end

%% **********  Parameters  *****************
% Demonstrations
decRate=20;     %Decimation rate
dem=7;          %Number of demonstrations

%model
l=3;		    % degree of nonlinearity.

%algorithm 
npoints=8; %number of points used in the  imposing the fixed points.
AlphaMin=20; %AlphaMin value given in percentage. In the function, AlphaMin is computed like AlphaMin=AlphaMin*0.01*alphaDistance-alphaDistance.
SEAMax=20; %SEAMax value given in percentage. In the function, SEAMax is computed like SEAMax=SEAMax*0.01*SEA+SEA.
MaxIt=5;  %Maximum number of iterations in each function
fat=5; % Used to calculate the distance applied in the imposition of fixed points using the formula fat*max(size in x of a cell, size in y of a cell)

%Data window
%The code works for maps where the number of rows is equal to the number of columns
nx=70; %number of cells in x of the map
ny=nx; %number of cells in y of the map 
windowSize=0.5; %Factor of increase of the window in relation to the area occupied by the data.
%Example:
% windowSize=1 increases the window containing the data by 100% by enlarging the x and y axis in all four directions.
% Initial data window  : xmin=0, xmax=10, ymin=0, ymax=10 
% Extended data window : xmin=-10, xmax=20, ymin=-10, ymax=20

%% ************************************************************
% LOAD DATA
[d, dt,mov,dmax]=loadData(decRate, dem);

%% ************************************************************
% Get information from the data window
window=dataWindow(d,dem,nx,ny,windowSize);
[baciadem, bd] = basinDem(d,dem,window.mx,window.my); 
c=[]; %vector with fixed points imposed

%% NAR-RM procedure

model=estimateModel(d,l);
plotAll('Model obtained with LS estimator',model,d,dt,1,window,c,dmax,bd);

[c,model]=getTargetAttractor(model,window,npoints,fat,d,baciadem,c);
plotAll('After getTargetAttractor fuction',model,d,dt,2,window,c,dmax,bd);

[c,model]=getRM_Model1(model,window,npoints,fat,d,baciadem,c,MaxIt);
[c,model]=getRM_Model2(model,window,npoints,fat,d,baciadem,bd,c,MaxIt,dmax);
plotAll('After getRM_Model (1 and 2) functions',model,d,dt,3,window,c,dmax,bd);

[c,model]=optimizeAlphaDistance(model,window,npoints,fat,d,baciadem,bd,c,MaxIt,dmax,SEAMax);
plotAll('After optimizeAlphaDistance function',model,d,dt,4,window,c,dmax,bd);

[c,model]=optimizeAccuracy(model,window,npoints,fat,d,baciadem,bd,c,MaxIt,dmax,AlphaMin);
plotAll('After optimizeAccuracy function',model,d,dt,5,window,c,dmax,bd);

%% Execution of the model
plotExecution('Execution of the RM model',model,d,dt,7,window);

disp('Click the image with the left mouse button to perform a simulation on the model. A right-click closes the simulations.');
modelExecution(7,model,dmax,1000); %1000 - number of iterations

%% Plotting the result
f=figure(6);
set(f,'Name','Final result');

h1=subplot('Position',[0.07, 0.3 ,0.40, 0.5 ]);
set(h1,'YTick',[]);
set(h1,'XTick',[]);
h2=subplot('Position',[0.57, 0.3 ,0.40, 0.5 ]);
set(h2,'YTick',[]);
set(h2,'XTick',[]);

figure(1);
hax1=gca;
pos=get(h1,'Position');
hax2=copyobj(hax1,figure(6));
set(hax2, 'Position', pos);

figure(5);
hax1=gca;
pos=get(h2,'Position');
hax2=copyobj(hax1,figure(6));
set(hax2, 'Position', pos);
