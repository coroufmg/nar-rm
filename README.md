# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
This repository contains:
#### 1 - the software implements the NAR-RM method proposed in the article "Learning robot reaching motions by demonstration using nonlinear autoregressive models". 
#### 2 - the NAR-RM files used in the benchmark "Open-source benchmarking for learned reaching motion generation in robotics"
#### 3 - link to videos of the experiments performed using NAR-RM on a mobile robot and an industrial manipulator.

### How do I get set up? ###
This software was developed for MATLAB. To run it, download all source files and run the NAR_RM.m script.

### Who do I talk to? ###
rsantos@unifei.edu.br or reyfow@gmail.com or gpereira@ufmg.br

