function [xd,ok]=NARRM(model,p,time)
%xd - the velocity computed by the dynamic system.
%ok - using to stop the simulation in the benchmark, where (0) stop de motion and (1) continue the motion

if (time>=1000 && norm(p)<1) || time >1500
    ok=0;
    xd=[0 0];
    return;
end

nx=size(model.px,1); %number of terms in the function x
ny=size(model.py,1); %number of terms in the function y

%Calculating the next point.
px=0;
for i=1:nx
    px=px+p(1)^model.px(i,1)*p(2)^model.px(i,2)*model.xx(i);
end

py=0;
for i=1:ny
    py=py+p(1)^model.py(i,1)*p(2)^model.py(i,2)*model.xy(i);
end


%If start point over fixed point. Disturb in x.
if time==1
    if px==p(1) && py==p(2)
        px=px+0.01;
    end
end

%Checks if the next point is inside the workspace. If the next point is outside
%the workspace, this means that execution is probably outside the target's
%basin of attraction. In this case, we reduce the execution velocity of the movement.
if px>model.janela.maxx || px<model.janela.minx || ...
        py>model.janela.maxy || py<model.janela.miny
    if time>=1000
        ok=0;
        xd=[0 0];
        return;
    end
    ang=atan2(py-p(2),px-p(1));
    px=p(1)+cos(ang)*0.001;
    py=p(2)+sin(ang)*0.001;
    %Calculating velocity.
    xd=diff([p(1) p(2); px py])/model.dt;
    ok=1;
    return;
end

%NAR-RM can generate high velocities at points distant from the demonstration
%area, %i.e., the next point may be distant. Therefore, we limit execution to
%twice the largest distance calculated using the demonstrations.
d2p=sqrt( (p(1)-px)^2 +  (p(2)-py)^2 );
if d2p>model.dmax*2
    ang=atan2(py-p(2),px-p(1));
    px=p(1)+cos(ang)*(model.dmax*2);
    py=p(2)+sin(ang)*(model.dmax*2);
end

%Calculating velocity.
xd=diff([p(1) p(2); px py])/model.dt;
ok=1;
end

