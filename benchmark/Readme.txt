This package contains data and codes used in the execution of the benchmark 
"Open-source benchmarking for learned reaching motion generation in robotics"
available in https://www.amarsi-project.eu/benchmark-framework

This source code include  3 subdirectories: 'Class', 'NARRM_lib' and 'LASA_Handwriting_Dataset'.

Class: contain class definition to NAR-RM.

NARRM_lib: contain auxiliar function to NAR-RM.

LASA_Handwriting_Dataset: contains the learned models using NAR-RM method for the LASA Handwriting Dataset.
