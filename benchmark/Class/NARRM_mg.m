classdef  NARRM_mg < MovementGenerator
    properties
        %properties in superclass
    end
    methods
        function p = NARRM_mg()
            p@MovementGenerator();
        end
        
        function init(p,model,dim)
            p.init@MovementGenerator(dim);
            p.learnedModel = model;
        end
        
        function xd = step(p)
            x_relative = p.curr_position-p.target_position;
            [xd,ok]=NARRM(p.learnedModel,x_relative,p.curr_time);
            if ok==0
                p.doStop();
            end
        end
    end
end