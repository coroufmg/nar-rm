function [model]=estimateModel(d,l)
[~,m]=size(d(1).MY);
dem=size(d,2);
%% ************************************************************
% GENERATION OF THE SET OF CANDITATE TERMS

disp('Generating the set of canditate terms...');
fprintf('Akaikem function: Looking for a better number of terms.\n');
[Cand,~] = mgenterm(l,2,1,0,0,0,0);
Cand=Cand(2:end,:); %retirando a constante do conjunto de candidatos
%fprintf('Numero de termos candidatos ap�s tirar a constante %d \n',size(Cand,1));
[~, mint] = akaikem(Cand,d,2);
fprintf('\nNumber of terms defined by the akaikem function.\nX %d terms\nY %d terms',mint(1),mint(2));

%% ************************************************************
% GENERATION OF THE MODEL

nprmd=mint;
nnt = zeros(1,m);
for i=1:m
	   nnt(1,i) = nprmd(i);
end;

model.nprmd=nprmd;
model.nnt=nnt;

fprintf('\n\nEstimating the model...\n');
[model.s(1).model,model.s(1).x,~,~]=morthreg(Cand,1,[],d,[nprmd(1) 0],0);
[model.s(2).model,model.s(2).x,~,~]=morthreg(Cand,2,[],d,[nprmd(2) 0],0);

s=model.s;

%montando o modelo MIMO
model.model=addmodel(s(1).model,s(2).model);
model.x=[s(1).x; s(2).x];

%obtendo o modelo simbolico
for j=1:2
    t3=eqdf(s(j).model(1:(nprmd(j)+0),:),s(j).x(1:(nprmd(j)+0),1),j);
    model.exp(j).simb=char(t3(1:end));
end;

%obtendo as matrizes do modelo
for k=1:2
    MY=d(1).MY;
    Pp=mbuild_p(s(k).model,d(1).MY,[]);
    tam=size(d(1).MY,1);
    for i=2:dem
        auxMY=d(i).MY;
        MY=[MY; auxMY(2:tam,:)];
        aux=mbuild_p(s(k).model,auxMY,[]);
        Pp=[Pp ; aux(2:tam,:)];
    end;
    model.R(k).aux=inv(Pp'*Pp);
    if(k==1)
        model.Ppx=Pp;
    else
        model.Ppy=Pp;
    end;
end;

end