function [cor]= msimodel2D(model,x0,nnt,MY0,janela,dmax,cor,n)
% function ys = msimodel(model,x0,nt,MY0,MU,ME) returns model predicted
%       output for the subsystems (model,x0,nnt)
%
% On entry
%	model
%	x0	- coefficients
%	nnt	- number of terms of each subsystem model
%	MY0	- matrix of initial conditions
%	MU	- matrix of input signals
%	ME	- matrix of noise signals
%
% On return
%	ys	- matrix of output signals

% Eduardo Mendes - 3/09/94
% ACSE - Sheffield

% Eduardo Saraiva - 16/02/98
% CPDEE - UFMG

%Rafael Santos - 18/06/2015
% PPGEE - UFMG

dx=janela.dx;
dy=janela.dy;
mx=janela.mx(1,1);
my=janela.my(1,1);



nsuby=size(nnt,2);

[a,degree]=size(model);

[npr,nno]=mget_inf(model,nnt);


y=zeros(2,nsuby);
y(1,:)=MY0(1,:);
px=MY0(1,1);
py=MY0(1,2);

lx=round((px-mx)/dx)+1;
ly=round((my-py)/dy)+1;

lxant=lx;
lyant=ly;

lista=[];
dmin=min(dx,dy)*0.25;

pos =ones(1,nsuby);
if nsuby > 1
    for i=2:nsuby
        pos(1,i) = pos(1,i-1) + npr(i-1) + nno(i-1);
    end;
end;





it=1;
teste=0;
tam=n*200;
while it<tam %5000
     y(2,:)=y(2,:)*0;
    for k=1:nsuby
        for i=pos(k):( pos(k) + npr(k) + nno(k) - 1)
            sinter1=x0(i,1);
            for j=1:degree
                a=model(i,j);
                if ((a == 0) || (a == (-1))), break; end;
                kk=floor(a/1000);
                kk1=floor((a-1000*kk)/100);
                
                if kk == 1
                    sinter1=sinter1*y(1,kk1);
                end;
            end;
            if (a == (-1)), break; end;
            y(2,k)=y(2,k)+sinter1;
        end;
    end;
    
    it=it+1;
    if isnan(y(2,1))||isnan(y(2,2))
        cor(ly,lx)=0;
        for i=1:size(lista,1)
            cor(lista(i,2),lista(i,1))=0;
        end
        return;
    end
    
    
     
    dist=sqrt((y(2,1)-y(1,1))^2 + (y(2,2)-y(1,2))^2 );
       
    if dist>dmax
        ang=atan2(y(2,2)-y(1,2),y(2,1)-y(1,1));
        y(2,1)=y(1,1)+cos(ang)*(dmax);
        y(2,2)=y(1,2)+sin(ang)*(dmax);
        dist=sqrt((y(2,1)-y(1,1))^2 + (y(2,2)-y(1,2))^2 );
    end
    
    if y(2,1)> janela.maxx || y(2,1)< janela.minx || y(2,2)> janela.maxy || y(2,2)< janela.miny
        cor(ly,lx)=0;
        for i=1:size(lista,1)
            cor(lista(i,2),lista(i,1))=0;
        end
        return;
    end
       
    lxn=round((y(2,1)-mx)/dx)+1;
    lyn=round((my-y(2,2))/dy)+1;
    
    pxc=mx(1,1)+(lxn-1)*dx;
    pyc=my(1,1)-(lyn-1)*dy;
    
    dd=sqrt((y(2,1)-pxc)^2 + (y(2,2)-pyc)^2 );
    
    if cor(lyn,lxn)~=2 && dd<dmin 
        cor(ly,lx)=cor(lyn,lxn);
        for i=1:size(lista,1)
           cor(lista(i,2),lista(i,1))=cor(lyn,lxn);
        end
       return;
    end
    

    
    if dd<dmin && (lxant~=lxn || lyant~=lyn)
        lista=[lista; [lxn lyn]];
        lxant=lxn;
        lyant=lyn;
    end
    
    if norm(y(2,:))>2 && dist<0.0001 %0.0001
        if teste==0 %saiu sobre um ponto fixo
            y(1,1)=y(1,1)+0.01;
            teste=1;
            continue;
        end
        cor(ly,lx)=0;
        for i=1:size(lista,1)
           cor(lista(i,2),lista(i,1))=0;
        end
        return;
    end
    
    
    
%     if norm(y(2,:))<0.05
%         cor(ly,lx)=1;
%         for i=1:size(lista,1)
%            cor(lista(i,2),lista(i,1))=1;
%         end
%         return;
%     end;
    
 
    teste=1;
    y(1,:)=y(2,:);    
end
%  fprintf('Maximo atingido: %d\n ',it);
cor(ly,lx)=0;
for i=1:size(lista,1)
    cor(lista(i,2),lista(i,1))=0;
end

end



