function [window]=dataWindow(d,dem,nx,ny,tamJanela)
%Retorna:
%   *as coordenadas da janela de dados em [ pfminx, pfmaxx, pfminy, pfmaxy ]
%   *a matriz mx onde cada celula possui a coordenada x do centro da c�lula
%   *a matriz my onde cada celula possui a coordenada y do centro da c�lula


%% **********************************************
% Impress�o das demonstra��es 

if nargin ==4  %calcula dx
    tamJanela=0.5;
end

plotDem(0,d,dem,10,'');
figure(10);
hold on;
axis('tight');
aux=axis;%pegando coordenadas da janela de demonstra��o 
pfmaxx=aux(2);
pfminx=aux(1);
pfmaxy=aux(4);
pfminy=aux(3);

distx=pfmaxx-pfminx;
disty=pfmaxy-pfminy;

%definindo o tamanho das janelas 

pfmaxx=pfmaxx+tamJanela*distx;
pfminx=pfminx-tamJanela*distx;
pfmaxy=pfmaxy+tamJanela*disty;
pfminy=pfminy-tamJanela*disty;


%centralizando (0,0)
%eixo x
dx=(pfminx-pfmaxx)/(nx-1);
dd=dx*(floor(pfminx/dx)-(pfminx/dx));
pfminx=pfminx+dd;
pfmaxx=pfmaxx+dd;
%eixo y
dy=(pfminy-pfmaxy)/(ny-1);
dd=dy*(floor(pfminy/dy)-(pfminy/dy));
pfminy=pfminy+dd;
pfmaxy=pfmaxy+dd;

axis([pfminx pfmaxx pfminy pfmaxy]);
hold off;
close(10);
pause(0.1);


ax_x=linspace(pfminx,pfmaxx,nx); %computing the mesh points along each axis
ax_y=linspace(pfminy,pfmaxy,ny); %computing the mesh points along each axis
[mx ,my]=meshgrid(ax_x,ax_y); %meshing the input domain

%coloca matriz no formato da imagem
auxy=zeros(size(mx));
for i=1:nx    
    auxy(i,:)=my(nx-i+1,:);
end;
my=auxy;

window.minx=pfminx;
window.maxx=pfmaxx;
window.miny=pfminy;
window.maxy=pfmaxy;
window.dx=mx(1,2)-mx(1,1);
window.dy=my(1,1)-my(2,1);
window.dist=max(window.dx,window.dy);
window.mx=mx;
window.my=my;
window.nx=nx;
window.ny=ny;

end