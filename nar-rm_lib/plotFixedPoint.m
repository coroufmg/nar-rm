function plotFixedPoint( S,tpf, ng, L, marca)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
figure(ng);
hold on;
lv=zeros(1,5);
ll={'Training Data'; 'Dynamic Flow';'Repellor';'Attractor';'Saddle'};
lv(1)=1;
lv(2)=1;

fprintf('\nPlotting Fixed Points\n\n');
for i=1:size(S.y_1,1)
    switch tpf(i)
        case 1
            L(4)=plot(S.y_1(i),S.y_2(i),[marca 'g'],'MarkerSize',7,'MarkerEdgeColor','k','MarkerFaceColor','g');
            lv(4)=1;
        case 2
            L(3)=plot(S.y_1(i),S.y_2(i),[marca 'r'],'MarkerSize',7,'MarkerEdgeColor','k','MarkerFaceColor','r');
            lv(3)=1;
        case 3
            L(5)=plot(S.y_1(i),S.y_2(i),[marca 'y'],'MarkerSize',7,'MarkerEdgeColor','k','MarkerFaceColor','y');
            lv(5)=1;
    end  
end;

LL=[];
LLN=[];
for i=1:5
    if lv(i)==1
        LL=[LL L(i)];
        LLN=[LLN; ll(i)];
    end;
end;    
legend(LL,LLN,'Orientation','horizontal','Location','northoutside');
%legend(LL,LLN);
hold off;
end

