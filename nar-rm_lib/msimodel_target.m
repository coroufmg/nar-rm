function y = msimodel_target(model,x0,nnt,MY0,n,janela)
% function y = msimodel(model,x0,nt,MY0,n,janela) returns model predicted
%       output for the subsystems (y)
%
% On entry
%	model
%	x0	- coefficients
%	nnt	- number of terms of each subsystem model
%	MY0	- matrix of initial conditions
%
% On return
%	y	- matrix of output signals

% Eduardo Mendes - 3/09/94
% ACSE - Sheffield

% Eduardo Saraiva - 16/02/98
% CPDEE - UFMG 

%Rafael Santos - 18/06/2015
% PPGEE - UFMG


nsuby=size(nnt,2);
[~,degree]=size(model);

[npr,nno,lag]=mget_inf(model,nnt);

y=zeros(n,nsuby);
y(1:lag,:)=MY0(1:lag,:);

pos =ones(1,nsuby);
if nsuby > 1
   for i=2:nsuby
      pos(1,i) = pos(1,i-1) + npr(i-1) + nno(i-1);
   end;
end;

for nn=lag+1:n
    for k=1:nsuby
        for i=pos(k):( pos(k) + npr(k) + nno(k) - 1)
            sinter1=x0(i,1);
            for j=1:degree
                a=model(i,j);
                if ((a == 0) | (a == (-1))), break; end;
                kk=floor(a/1000);
                kk1=floor((a-1000*kk)/100);
                kk2=a-1000*kk-100*kk1;
                if kk == 1
                    sinter1=sinter1*y(nn-kk2,kk1);            
                end;
            end;
            if (a == (-1)), break; end;
            y(nn,k)=y(nn,k)+sinter1;
        end;
    end;
    if  y(nn,1)> janela.maxx || y(nn,1)< janela.minx || ... 
        y(nn,2)> janela.maxy || y(nn,2)< janela.miny 
        y(nn,:)=nan;
        return;
    end
end;


if(sum(isnan([y(nn,1),y(nn,2)]))>0)
    return;
end

ys=zeros(2,nsuby);
ys(1,:)=y(n,:);


nn=2;

cont=0;
max=n*5;

while true
    for k=1:nsuby
        for i=pos(k):( pos(k) + npr(k) + nno(k) - 1)
            sinter1=x0(i,1);
            for j=1:degree
                a=model(i,j);
                if ((a == 0) || (a == (-1))), break; end;
                kk=floor(a/1000);
                kk1=floor((a-1000*kk)/100);
                
                if kk == 1
                    sinter1=sinter1*ys(nn-1,kk1);
                end;
            end;
            if (a == (-1)), break; end;
            ys(nn,k)=ys(nn,k)+sinter1;
        end;
    end;
    
    dist=sqrt((ys(nn,1))^2 + (ys(nn,2))^2);
    
     if dist<0.5 %adicionar janela.dmin para ficar gen�rico
        return;
    end;
    
    
    if isnan(dist) || ys(nn,1)> janela.maxx || ys(nn,1)< janela.minx || ...
                      ys(nn,2)> janela.maxy || ys(nn,2)< janela.miny 
        y(n,:)=nan;
        return;
    end
    
    if cont>max
       y(n,:)=nan;
       return;
    end
    
    cont=cont+1;
    y(n+cont,:)=ys(nn,:);
   
    ys(nn-1,:)=ys(nn,:);
    ys(nn,:)=ys(nn,:)*0;
    
end

