function [alpha,p1,p2]=calcAlpha(cor,mx,my,bd,dem)

dx=mx(1,2)-mx(1,1);
dy=my(1,1)-my(2,1);

nx=size(mx,1);
ny=size(my,1);

min=dx*nx+dy*ny;%max([dx*nx,dy*ny,dz*nz]);
mm=min;
for lx=1:nx
    for ly=1:ny
        if cor(ly,lx)==1
            ok=testa(lx,ly,nx, ny,cor);
            if ok==1
                [menor,p1m,p2m]=distancias(lx,ly,mx,my,dx,dy,mm,bd,dem);
                if menor <min
                    p1=p1m;
                    p2=p2m;
                    min=menor;
                end
            end
        end
    end
end

alpha=min;
end

function ok=testa(lx, ly, nx, ny, cor)
ok=0;
for x=lx-1:lx+1
    for y=ly-1:ly+1
            if x>0 && y>0 && x<=nx && y<=ny 
                if cor(y,x)~=1
                    ok=1;
                    return;
                end
            else
                ok=1;
            end
    end
end
end

function [menor,p1,p2]=distancias(lx,ly,mx,my,dx,dy,menorini,bd,dem)

px=mx(1,1)+(lx-1)*dx;
py=my(1,1)-(ly-1)*dy;

p1=[px py];
menor=menorini;
for i=1:dem
    tam=size(bd(i).p,1);
    for j=1:tam
        lx=bd(i).p(j,1);
        ly=bd(i).p(j,2);
        px2=mx(1,1)+(lx-1)*dx;
        py2=my(1,1)-(ly-1)*dy;

        dist=sqrt((px-px2)^2+(py-py2)^2);
        if menor> dist
            menor=dist;
            p2=[px2 py2];
        end
    end
end
end
    
