function area = areaquadrilatero(xv,yv)
%UNTITLED Summary of this function goes here
% Calcula a �rea de um quadrilatero dado por um polinomio que pode ter auto-interse��o
%entrada: ordem dos pontos 
%       p2  *-----------* p4 
%           |           |
%           |           |
%        p1 *-----------*p3

%  fill([xv(1), xv(2), xv(4), xv(3),xv(1)],[yv(1), yv(2), yv(4), yv(3),yv(1)],[190/255 255/255 135/255]);
%  plot([xv(1), xv(2), xv(4), xv(3),xv(1)],[yv(1), yv(2), yv(4), yv(3),yv(1)],'k');
%axis([-3 3 -3 3]);
    
%testa se tem interse��o e o ponto de interse��o
[x,y]=polyxpoly(xv(1:2),yv(1:2),xv(3:4),yv(3:4));
if size(x,1)==0
    %n�o tem interse��o
    area=polyarea([xv(1), xv(2), xv(4), xv(3),xv(1)],[yv(1), yv(2), yv(4), yv(3),yv(1)]);
else
%     disp('tem interse��o');
    area=polyarea([xv(1),x,xv(3),xv(1)],[yv(1),y,yv(3),yv(1)]);
    area= area + polyarea([xv(2),x,xv(4),xv(2)],[yv(2),y,yv(4),yv(2)]);
end;



end

