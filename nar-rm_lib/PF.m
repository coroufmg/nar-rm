function [Simb,jacob,tpf, atrator, alvo]=PF(exp,imp,c)
%t = cputime;
if (nargin ==1)
    imp=1;
end

ex1=simplify(sym(exp(1).simb));
ex2=simplify(sym(exp(2).simb));
Simb=solve(ex1,ex2, 'Real', true);

 
for j=1:size(c,1)
    px=c(j,1);
    py=c(j,2);
    if verifica(Simb,px,py)==0
        Simb.y_1=[Simb.y_1 ; px];
        Simb.y_2=[Simb.y_2 ; py];
    end
end


%-----------------------------------
%testa se (0,0) faz parte de Simb, ou seja, um ponto fixo
testesimb=0;
for i=1:size(Simb.y_1,1)
    if(Simb.y_1(i)==0 && Simb.y_2(i)==0)
        %trocando o alvo para o fim da lista
        Simb.y_1(i)=Simb.y_1(size(Simb.y_1,1));
        Simb.y_2(i)=Simb.y_2(size(Simb.y_1,1));
        Simb.y_1(size(Simb.y_1,1))=0;
        Simb.y_2(size(Simb.y_1,1))=0;
        testesimb=1;
        break;
    end;
end;
if testesimb==0 %adiciona o ponto (0,0)
    if imp==1
        disp('Ponto fixo (0,0) nao encontrado pelo (solve). Adicionado.');
    end
    Simb.y_1=[Simb.y_1 ; 0];
    Simb.y_2=[Simb.y_2 ; 0];
end;
%-----------------------------


%tirando pontos complexos de simb
j=1;
tam=size(Simb.y_1,1);
% if imp==1
%     fprintf('Numero de pontos fixos encontrados: %d\n',tam);
% end
for i=1:tam
    if isreal(Simb.y_1(i)) && isreal(Simb.y_2(i))
        aux.y_1(j,1)=Simb.y_1(i);
        aux.y_2(j,1)=Simb.y_2(i);
        j=j+1;
    end;
end;
Simb=aux;
if imp==1
    fprintf('Number of fixed points found: %d\n\n',size(Simb.y_1,1));
end

%Avaliando os pontos fixos
rf(1)=sym(exp(1).simb(6:end));
rf(2)=sym(exp(2).simb(6:end));
syms y_1 y_2;
varia=[y_1; y_2];
jacob=jacobian(rf,varia);

atrator=[];
tpf=zeros(size(Simb.y_1,1),1); %atrator = 1, repulsor =2 , sela =3  complexo= 0
a=1;
for i=1:size(Simb.y_1,1)
    if isreal(Simb.y_1(i)) && isreal(Simb.y_2(i))
        y_1=Simb.y_1(i);
        y_2=Simb.y_2(i);
        av=double(eig(eval(jacob)));
        if imp==1
            fprintf('x = %s  y = %s  av1 = %s  av2 = %s ',num2str(double(y_1)),num2str(double(y_2)), num2str(av(1)), num2str(av(2)));
        end
        if isreal(av(1))
            av(1)=abs(av(1));
            av(2)=abs(av(2));
            if av(1)>1 && av(2)>1% autovalores positivo (repulsor)
                tpf(i)=2;
                if imp==1
                    fprintf(' [ Repellor ]\n\n');
                end
            elseif av(1)<1 && av(2)<1 %autovalores negativo (atrator)
                if imp==1
                    fprintf(' [ Attractor ]\n\n');
                end
                tpf(i)=1;
                atrator(a)=i;
                a=a+1;
            else %ponto de sela
                tpf(i)=3;
                if imp==1
                    fprintf(' [ Saddle ]\n\n');
                end
            end;
        else
            %av(3)=sqrt(real(av(1))^2+imag(av(1))^2);
            av(3)=abs(av(1));
            if av(3)>1
                tpf(i)=2;
                if imp==1
                    fprintf(' [ Repellor  ]\n\n');
                end
            elseif av(3)<1
                tpf(i)=1;
                if imp==1
                    fprintf(' [ Attractor]\n\n');
                end
                atrator(a)=i;
                a=a+1;
            else
                if imp==1
                    fprintf(' [ Center ]\n\n');
                end
            end;
        end;
    end;
end;

alvo=target(exp);
end



function resp=verifica(Simb,px,py)
t=size(Simb.y_1,1);
for i=1:t
    if sqrt((double(Simb.y_1(i))-px)^2+(double(Simb.y_2(i))-py)^2)<0.5
        resp=1;
        return;
    end
end
resp=0;
end