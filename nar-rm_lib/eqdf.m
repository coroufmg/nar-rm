function t = eqdf(model,x,sy,prec)
% function t = eq(model,x,prec) returns the discodified model 
%
% On entry
%	model 
%	x	- coefficients
%	sy	- subsystem number
%	prec	- precision
%
% On return
%	t	- discodified model

% Eduardo Mendes - 16/05/94
% ACSE - Sheffield

% Eduardo Saraiva - 30/04/97
% CPDEE-UFMG

%Rafael Santos - 18/06/2015
% PPGEE - UFMG

flag = 0;

if ((nargin < 3) | (nargin > 4))
	error('eq requires 3 or 4 input arguments.');
elseif nargin ==3
	prec=5;
end;

[nt,n2]=size(model);

[a,aa]=size(x);

if (a ~= nt)
	error('model and x must have the same number of columns.');
end;

% Calculations

[npr,nno]=mget_inf(model);

nte=npr+nno;

t='';

c='';
f='';
j=1;

a=sprintf('y_%g = ',sy);
% d=sprintf('Subsystem (%g)',sy);

%Termos
%sen y 4000  cos y 5000
%sen u 6000  cos u 7000

for i=1:nte
	b=model(i,(j-1)*n2+1:n2*j);
	kk=floor(b/1000);
	kk1=floor((b-kk*1000)/100); % subsystem
	kk2=floor((b-kk*1000-kk1*100)); % lag
    
	b=f;
	e=f;
    raf=0;
    for k=1:n2
        if kk(k) == 1
            if raf==0
               b=[b sprintf(' * y_%g',kk1(k))];
               raf=1;
            else
                b=[b sprintf(' * y_%g',kk1(k))];
            end;
        elseif kk(k) == 2
            b=[b sprintf(' * u_%g(k-%g)',kk1(k),kk2(k))];
            
        elseif kk(k) == 3
            b=[b sprintf(' * e_%g(k-%g)',kk1(k),kk2(k))];
            
        elseif kk(k) == 4
            b=[b sprintf(' * sin(y_%g(k-%g))',kk1(k),kk2(k))];
            
        elseif kk(k) == 5
            b=[b sprintf(' * cos(y_%g(k-%g))',kk1(k),kk2(k))];
            
        elseif kk(k) == 6
            b=[b sprintf(' * sin(u_%g(k-%g))',kk1(k),kk2(k))];
            
        elseif kk(k) == 7
            b=[b sprintf(' * cos(u_%g(k-%g))',kk1(k),kk2(k))];
            
        elseif kk(k) == 0
            
            break;
        end;
    end;
	if i ~= nte
		if i == 1
			if flag 
				b=[a b ' '];
			else
				b=[a eq1(x(i,j),prec) b ' '];
			end;
		else
			if flag
				b=[c b ' '];
			else
				b=[c eq1(x(i,j),prec) b ' '];
			end;
		end;
	else
		if i == 1
			if flag
				b=[a b];
			else
				b=[a eq1(x(i,j),prec) b];
			end;
		else
			if flag 
				b=[c b];
			else
				b=[c eq1(x(i,j),prec) b];
			end;
		end;
	end;
	%t=str2mat(t,b);
	t=[t b];
end;
