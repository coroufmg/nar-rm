function cor=basinAttraction(window,model,x,dmax,n)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

cor=ones(size(window.mx))*2;

px=0;
py=0;

[lx,ly]=pointToCell(px,py,window);

cor(ly,lx)=1;

ce=lx; cd=lx; ls=ly; li=ly;
cea=1; cda=1; lsa=1; lia=1;

while ce>1 || cd<window.nx || ls>1 || li <window.ny
    if ce>1
        ce=ce-1;
    else
        cea=0;
    end;
    
    if cd<window.nx
        cd=cd+1;
    else
        cda=0;
    end;
    
    if ls>1
        ls=ls-1;
    else
        lsa=0;
    end;
    
    if li<window.ny
        li=li+1;
    else
        lia=0;
    end;
    
    if cea==1
        for j=ls:li
            if cor(j,ce)==2
            [px,py]=cellToPoint(ce,j,window);
            [cor]=msimodel2D(model.model,x,model.nnt,[px py],window,dmax,cor,n);
            end
        end;
    end;
    if cda==1
        for j=ls:li
            if cor(j,cd)==2
            [px,py]=cellToPoint(cd,j,window);
            [cor]=msimodel2D(model.model,x,model.nnt,[px py],window,dmax,cor,n);
            end;
        end;
    end;
    if lsa==1
        for j=ce:cd
            if cor(ls,j)==2
            [px,py]=cellToPoint(j,ls,window);
            [cor]=msimodel2D(model.model,x,model.nnt,[px py],window,dmax,cor,n);
            end
        end;
    end;
    if lia==1
        for j=ce:cd
            if cor(li,j)==2
            [px,py]=cellToPoint(j,li,window);
            [cor]=msimodel2D(model.model,x,model.nnt,[px py],window,dmax,cor,n);
            end;
        end;
    end;
   
end;

end

