function [c,model]=optimizeAlphaDistance(model,window,npontos,fat,d,baciadem,bd,c,MaxIt,dmax,SEAMax)
%Esta fun��o tenta aumentar a valor da dist�ncia alfa permitindo uma piora m�xima de 20 porcento do valor do SEA

dist=window.dist;
dem=size(d,2);
[SEA,~,~,~]= sea(model, model.x, d, dem,window);

[Simb,~,tpf,~,~]=PF(model.exp,0,c);

n=size(d(1).MY,1);

cor=basinAttraction(window,model,model.x,dmax,n); 
rm=checkRM(d,dem,model,cor,bd,window);
[dalpha,~,~]=calcAlpha(cor,window.mx,window.my,bd,dem);

porSEA=SEAMax;
SEAMax=SEA*(1+porSEA/100);

fprintf('\n\n ||| Performing the function optimizeAlphaDistance  ||| \n\n');
fprintf('[ Initial SEA = %f  ( SEAmax = %f )] \n',SEA,SEAMax);
fprintf('Initial Alpha-distance = %f\n\n',dalpha);

melhor.c=c;
melhor.exp=model.exp;
melhor.x=model.x;
melhor.dalpha=dalpha;
melhor.SEA=SEA;
melhor.cor=cor;
melhor.Simb=Simb;
melhor.tpf=tpf;

it=0;
teste1pf=0;
while it<MaxIt %enquanto n�o tiver RO ou atingir o valor m�ximo de itera��es
    %----------------------------------------------------------------------
    %Definindo os par�metros utilizados na fun��o deslocaPontoCirc
%     op=0;
%     op2=0;
    np = pontosJanela(Simb,window.mx,window.my);
    op=1; %Pontos dentro da janela
    op2=0; % Fora da bacia
    if np==1 %so tem um ponto fixo dentro da janela.
        op=0; %Pontos dentro da janela
        op2=0; % Fora da bacia
    end
    %----------------------------------------------------------------------    
    it=it+1;
    fprintf('iteration %d [MAXit=%d]\n',it,MaxIt);
    tam=size(Simb.y_1,1);
    k=0;
    vsis=[];
    fprintf('\nImposing fixed points  ' );
    %[ns,~]=nsaddles(Simb,mx,my,tpf);
    for i=1:tam
%         if ns>0 && tpf(i)~=3
%         if tpf(i)~=3
%             continue;
%         end
        px=double(Simb.y_1(i));
        py=double(Simb.y_2(i));
        if(px~=0 || py~=0) % ponto fixo n�o � o alvo
            k=k+1;
            fprintf('( %.2f, %.2f ) ',px,py);
            sis=imposeFP(model,window,npontos,op,op2,px,py,window.dist*fat,c,baciadem,cor);
            fprintf('[%d]',size(sis,2));
            if size(sis,2)==0
                k=k-1;
            else
                vsis=[vsis sis];
            end
        end
    end
    fprintf('\n');
    
    if size(Simb.y_1,1)==1
        if teste1pf ==0
            %fprintf('\n\n #### Tentando adicionar pontos fixos distantes do alvo #### \n\n');
            distancia=dist*size(mx,1)/2;
            op=0; %qualquer lugar
            op2=1; % fora da bacia
            vsis=imposeFP(model,window,npontos,op,op2,0,0,distancia,c,baciadem,cor);
            if size(vsis,2)==0
                k=0;
            else
                k=1;
            end
            teste1pf=1;
        end
    end
    
    
    
    if(k==0)
        disp('\nNo model found\n');
        if size(Simb.y_1,1)==1 %Sistema com um �nico ponto fixo atrator
            break;
        end;
        if fat>1
            %fat=fat-1;
            fat=fat-2;
            fprintf('Reducing distance from %f to %f\n',dist*(fat+1),dist*fat);
            disp('Coming back to the best model found.\n');
            %Atualizando para o melhor sistema obtido at� o momento
            c=melhor.c;
            model.exp=melhor.exp;
            model.x=melhor.x;
            dalpha=melhor.dalpha;
            SEA=melhor.SEA;
            Simb=melhor.Simb;
            tpf=melhor.tpf;
            cor=melhor.cor;
            model.s=atualizaSx(model.s,model.x);
            
            continue;
        else
            break;
        end;
    end
    
     [sis, cor_aux, Simb_aux,SEA_aux,dalpha_aux,tpf_aux]=melhorSistema4(vsis,model,d,dem,SEAMax,dalpha,window,c,dmax,bd);
     if dalpha_aux==-1
         fprintf('No system that improves Alpha-distance has been found\n\n');
         if fat>1
            %fat=fat-1;
            fat=fat-2;
            fprintf('Reducing distance from %f to %f\n',dist*(fat+1),dist*fat);
            disp('Coming back to the best model found.');
            %Atualizando para o melhor sistema obtido at� o momento
            c=melhor.c;
            model.exp=melhor.exp;
            model.x=melhor.x;
            dalpha=melhor.dalpha;
            SEA=melhor.SEA;
            Simb=melhor.Simb;
            tpf=melhor.tpf;
            cor=melhor.cor;
            model.s=atualizaSx(model.s,model.x);
            
            continue;
        else
            break;
        end;
     end
     
     c=sis.c;
     disp('Fixed point chosen');
     disp(c);
     
     model.exp=sis.exp;
     model.x=sis.x;
     model.s=atualizaSx(model.s,model.x);
     cor=cor_aux;
     Simb=Simb_aux;
     tpf=tpf_aux;
     SEA=SEA_aux;
     dalpha=dalpha_aux;
     fprintf('\n [ Alpha-distance %f  SEA %f ] \n\n',dalpha,SEA);
     
     SEAmax_aux=SEA*(1+porSEA/100);
     if SEAmax_aux < SEAMax
         fprintf('\nSEAmax updated from %f to %f \n\n',SEAMax,SEAmax_aux);
         SEAMax=SEAmax_aux;
     end;
     
     %gravando melhor
     
     if dalpha >melhor.dalpha
         melhor.c=c;
         melhor.exp=model.exp;
         melhor.x=model.x;
         melhor.dalpha=dalpha;
         melhor.SEA=SEA;
         melhor.cor=cor;
         melhor.Simb=Simb;
         melhor.tpf=tpf;
%          fprintf('Atualizando sistema\n');
%          disp('c');
%          disp(melhor.c);
%          disp('simb')
%          disp([Simb.y_1 Simb.y_2]);
%          [Simb2,~,~,~,~]=PF(melhor.exp,0,c);
%          disp('simb verificado')
%          disp([Simb2.y_1 Simb2.y_2]);
     elseif dalpha == melhor.dalpha
         if SEA < melhor.SEA
             melhor.c=c;
             melhor.exp=model.exp;
             melhor.x=model.x;
             melhor.dalpha=dalpha;
             melhor.SEA=SEA;
             melhor.cor=cor;
             melhor.Simb=Simb;
             melhor.tpf=tpf;
%              fprintf('Atualizando sistema\n');
%          disp('c');
%          disp(melhor.c);
%          disp('simb')
%          disp([Simb.y_1 Simb.y_2]);
%          [Simb2,~,~,~,~]=PF(melhor.exp,0);
%          disp('simb verificado')
%          disp([Simb2.y_1 Simb2.y_2]);
         end
     end
     
end

fprintf('\n\n Best \n');
fprintf('SEA = %f   Alpha-distance = %f\n\n',melhor.SEA,melhor.dalpha);
c=melhor.c;
model.exp=melhor.exp;
model.x=melhor.x;
model.s=atualizaSx(model.s,model.x);

            
end


function [melhor, cor,Simb,SEA,dalpha,tpf]=melhorSistema4(sis,model,d,dem,SEAmax,Mdalpha,window,c,dmax,bd)
n=size(d(1).MY,1);
tam =size(sis,2);

disp('Chosing the best model');

VSEA=ones(1,tam)*NaN;
Vdalpha=ones(1,tam)*(-1);
fprintf('Evaluating the model (Total %d) [ ',tam);
for k=1:tam
    x=sis(k).x;
    [SEA,~,~,~]=  sea(model, x,d, dem,window);
    if SEA <SEAmax
        fprintf('%d(B ',k); 
        cor=basinAttraction(window,model,x,dmax,n);
        rm=checkRM(d,dem,model,cor,bd,window);        
        if rm==1
            [dalpha,~,~]=calcAlpha(cor,window.mx,window.my,bd,dem);
            fprintf('%.2fda ) ',dalpha);
            if dalpha >=Mdalpha
                Vdalpha(k)=dalpha;
                VSEA(k)=SEA;
                V(k).cor=cor;
            end
        else
            fprintf(') ');
        end
    else
        fprintf('%d ',k); 
    end
    
end

fprintf(']\n\n');
[maxdalpha,k]=max(Vdalpha);

if maxdalpha==-1
    melhor=[];
    cor=[];
    Simb=[];
    tpf=[];
    SEA=NaN;
    dalpha=-1;
    return;
end

%Buscar o que tiver melhor SEA com o melhor dalpha
melhorSEA=VSEA(k);
for i=1:tam
    if Vdalpha(i)==maxdalpha
        if VSEA(i)<melhorSEA
            k=i;
            melhorSEA=VSEA(i);
        end
    end
end
%melhor escolhido em k
melhor=sis(k);
cor=V(k).cor;
[Simb,~,tpf,~,~]=PF(sis(k).exp,0,c);
SEA=VSEA(k);
dalpha=Vdalpha(k);
end




