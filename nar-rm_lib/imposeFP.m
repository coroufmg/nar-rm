function sis=imposeFP(model,window,npontos,op,op2,px_ini,py_ini,dist,c,baciadem,cor)
%Desloca pontos fixos e retorna em sis sistemas onde o alvo � um atrator e atendem o crit�rio de vari�ncia
%Entrada:
%        npontos = N�mero de pontos
%        op => ponto a ser deslocado
%              0 - qualquer ponto
%              1 - dentro da janela
%
%        op2 => pontos avaliados ap�s o deslocamento
%              0  - qualquer lugar
%              1  - fora da bacia
%              2  - dentro da janela e fora da bacia


sis=[];

if ((nargin < 11))
    cor=[];
end

nx=window.nx;
ny=window.ny;

[lx,ly]=pointToCell(px_ini,py_ini,window);
if (op==1)&& (lx>nx || ly>ny || lx<1 || ly<1) %verifica se o ponto a ser deslocado est� dentro da janela
    return;
end

%fprintf('\n[ Deslocando o ponto ( %f, %f ) ]\n\n',px,py);

%testa se o ponto (px, py) pertence a c, caso positivo, retiramos ele de c
%pois ele vai ser deslocado
t=size(c,1);
for i=1:t
    if sqrt((c(i,1)-px_ini)^2+(c(i,2)-py_ini)^2)<0.5
    %if abs(c(i,1)-px_ini)<0.5 && abs(c(i,2)-py_ini)<0.5
        %disp('Ponto esta em C. Retirando');
        c=[c(1:i-1,:); c(i+1:end,:)];
        break;
    end
end


%sorteando npontos
th=0:pi/(npontos/2):2*pi;
th=th(1:end-1);

pf(:,1)=dist * cos(th) + px_ini;
pf(:,2)= dist * sin(th) + py_ini;

cont=1;
for i=1:npontos
    caux=c;
    px=pf(i,1);
    py=pf(i,2);
    [lx,ly]=pointToCell(px,py,window);
    
    %evitar pontos no meio das demonstra��es
    if lx>0 && ly>0 && lx<=nx && ly<=ny && baciadem(ly,lx)~=0
        continue;
    end
    
    %evitar pontos dentro da bacia
    if (op2==1) && lx>0 && ly>0 && lx<=nx && ly<=ny && cor(ly,lx)~=0
        continue;
    end
    
    %dentro da janela e fora da bacia
    if (op2==2)
        if lx>0 && ly>0 && lx<=nx && ly<=ny && cor(ly,lx)~=0 %esta na bacia
            continue;
        end
        if (lx>nx || ly>ny || lx<1 || ly<1) % fora da janela
            continue;
        end
    end
    
    %fprintf('Avaliando o ponto na posicao ( %f, %f )\n',px,py);
    caux=[caux; px py];
    %monta as restri��es
    for k=1:2
        S=[];
        for j=1:size(caux,1)
            S = [S; makeFPConstraint(model.s(k).model,caux(j,1),caux(j,2))];
        end
        x=model.s(k).x(:,1);
        nt(k).theta=x-model.R(k).aux*S'*inv(S*model.R(k).aux*S')*(S*x-caux(:,k));
        t3=eqdf(model.s(k).model(1:(model.nprmd(k)+0),:),nt(k).theta(1:(model.nprmd(k)+0),1),k);
        exp(k).simb=char(t3(1:end));
    end;
    %montando novo x do modelo MIMO
    if sum(isnan(nt(1).theta))+sum(isnan(nt(2).theta))+sum(isinf(nt(1).theta))+sum(isinf(nt(2).theta)) >0 %erro de precisao no modelo
        continue
    end
    x=[nt(1).theta;nt(2).theta];
    alvo=target(exp);
    if alvo ==1 
        sis(cont).x=x;
        sis(cont).alvo=alvo;
        sis(cont).exp=exp;
        sis(cont).c=caux;
        cont=cont+1;        
    end
end

end

