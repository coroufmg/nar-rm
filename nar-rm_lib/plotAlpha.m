function plotAlpha(ng,p1,p2,pfminx, pfmaxx, pfminy, pfmaxy)
figure(ng);
hold on;

% dist=sqrt((p1(1)-p2(1))^2 + (p1(2)-p2(2))^2 );
% 
% ang=atan2(p2(2)-p1(2),p2(1)-p1(1));
% pxm=p1(1)+cos(ang)*(dist/2);
% pym=p1(2)+sin(ang)*(dist/2);

pxm=(p1(1)+p2(1))/2;
pym=(p1(2)+p2(2))/2;

plot([p1(1) p2(1)],[p1(2) p2(2)],'k',[p1(1) p2(1)],[p1(2) p2(2)],'k+', 'LineWidth',1);

if abs(p1(1)-p2(1))< abs(p1(2)-p2(2))
   text(pxm+3,pym,'\fontsize{13}\alpha');
else
   text(pxm,pym+3,'\fontsize{13}\alpha');
end
axis([pfminx pfmaxx pfminy pfmaxy]);
box on;
hold off;
end