function L = plotDem(op,d,dem,ng,titulo,pfminx, pfmaxx, pfminy, pfmaxy)
[n,~]=size(d(1).MY);
figure(ng);

if op ==0
    L(1)=plot(d(1).MY(1:n,1), d(1).MY(1:n,2),'r');
    L(2)=plot(d(1).MY(1:n,1), d(1).MY(1:n,2),'b');
    hold on;
else
    hold on;
    L(1)=plot(d(1).MY(1:n,1), d(1).MY(1:n,2),'r');
    L(2)=plot(d(1).MY(1:n,1), d(1).MY(1:n,2),'b');
end


for i=1:dem
   L(1)=plot(d(i).MY(:,1), d(i).MY(:,2),'r');
end;
title(titulo); %grid;
ylabel('$y$','interpreter','latex','fontsize',15);
xlabel('$x$','interpreter','latex','fontsize',15);
if ((nargin ==9))
    axis([pfminx pfmaxx pfminy pfmaxy]);
end
hold off;
end

