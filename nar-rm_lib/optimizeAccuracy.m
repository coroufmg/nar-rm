function [c,model]=optimizeAccuracy(model,window,npontos,fat,d,baciadem,bd,c,MaxIt,dmax,AlphaMin)
                                    
dist=window.dist;
dem=size(d,2);
[SEA,~,~,~]= sea(model, model.x, d, dem,window);

[Simb,~,~,~,~]=PF(model.exp,0,c);

n=size(d(1).MY,1);

cor=basinAttraction(window,model,model.x,dmax,n); 
rm=checkRM(d,dem,model,cor,bd,window);
[dalpha,~,~]=calcAlpha(cor,window.mx,window.my,bd,dem);

porAlfa=AlphaMin;
AlphaMin=dalpha*(1-porAlfa/100);

fprintf('\n\n ||| Performing the function optimizeAccuracy  ||| \n\n');
fprintf('\nInitial Alpha-distance = %f  ( AlphaMin = %f )\n',dalpha, AlphaMin);



fprintf('[ Initial SEA %f ] \n\n',SEA);

melhor.c=c;
melhor.exp=model.exp;
melhor.x=model.x;
melhor.dalpha=dalpha;
melhor.SEA=SEA;
melhor.cor=cor;
melhor.Simb=Simb;

it=0;
teste1pf=0;
while it<MaxIt %enquanto n�o tiver RO
    
%     op=0;
%     op2=0;
    np = pontosJanela(Simb,window.mx,window.my);
    op=1; %Pontos dentro da janela
    op2=0; % Fora da bacia
    if np==1 %so tem um ponto fixo dentro da janela.
        op=0; %Pontos qualquer lugar
        op2=0; % Fora da bacia
    end

    it=it+1;
    fprintf('iteration %d [MAXit=%d]\n',it,MaxIt);
    tam=size(Simb.y_1,1);
    k=0;
    vsis=[];
    fprintf('\nImposing fixed points ' );
    for i=1:tam
        px=double(Simb.y_1(i));
        py=double(Simb.y_2(i));
        if(px~=0 || py~=0) %n�o � o alvo
            k=k+1;
            fprintf('( %.2f, %.2f ) ',px,py);
            sis=imposeFP(model,window,npontos,op,op2,px,py,window.dist*fat,c,baciadem,cor);
            fprintf('[%d]',size(sis,2));
            if size(sis,2)==0
                k=k-1;
            else
                vsis=[vsis sis];
            end
            
        end
    end
    fprintf('\n');
    
    if size(Simb.y_1,1)==1
        if teste1pf ==0
            %fprintf('\n\n #### Tentando adicionar pontos fixos distantes do alvo #### \n\n');
            distancia=dist*size(mx,1)/2;
            op=0; %Pontos dentro da janela
            op2=0; % Fora da bacia e dentro da janela
            vsis=imposeFP(model,window,npontos,op,op2,0,0,distancia,c,baciadem,cor);
            if size(vsis,2)==0
                k=0;
            else
                k=1;
            end
            teste1pf=1;
        end
    end
    
    
    if(k==0)
         disp('\nNo model found\n');
        if fat>1
%             fat=fat-1;
            fat=fat-2;
            fprintf('Reducing distance from %f to %f\n',dist*(fat+1),dist*fat);
            disp('Coming back to the best model found.');
            %Atualizando para o melhor sistema obtido at� o momento
            c=melhor.c;
            model.exp=melhor.exp;
            model.x=melhor.x;
            dalpha=melhor.dalpha;
            SEA=melhor.SEA;
            Simb=melhor.Simb;
            cor=melhor.cor;
            model.s=atualizaSx(model.s,model.x);
            
            continue;
        else
            break;
        end;
    end
    
     [sis, cor_aux, Simb_aux,SEA_aux,dalpha_aux]=melhorSistema3(vsis,model,d,dem,SEA,AlphaMin,window,c,dmax,bd);
     if isnan(SEA_aux)
         fprintf('No system that improves SEA has been found\n\n');
         if fat>1
%             fat=fat-1;
            fat=fat-2;
            fprintf('Reducing distance from %f to %f\n',dist*(fat+1),dist*fat);
            disp('Coming back to the best model found.');
            %Atualizando para o melhor sistema obtido at� o momento
            c=melhor.c;
            model.exp=melhor.exp;
            model.x=melhor.x;
            dalpha=melhor.dalpha;
            SEA=melhor.SEA;
            Simb=melhor.Simb;
            cor=melhor.cor;
            model.s=atualizaSx(model.s,model.x);
            
            continue;
        else
            break;
        end;
     end
     
     
     c=sis.c;
     disp('Fixed point chosen');
     disp(c);
     model.exp=sis.exp;
     model.x=sis.x;
     model.s=atualizaSx(model.s,model.x);
     cor=cor_aux;
     Simb=Simb_aux;
     SEA=SEA_aux;
     dalpha=dalpha_aux;
     fprintf('\n [ SEA  %f  Alpha-distance = %f ] \n\n',SEA,dalpha_aux);
         
     dalphamin_aux=dalpha*(1-porAlfa/100);
     if dalphamin_aux > AlphaMin
          fprintf('\nAlphaMin updated from %f to %f \n\n',AlphaMin,dalphamin_aux);
          AlphaMin=dalphamin_aux;
     end;
         
     
     if SEA < melhor.SEA
         melhor.c=c;
         melhor.exp=model.exp;
         melhor.x=model.x;
         melhor.dalpha=dalpha;
         melhor.SEA=SEA;
         melhor.dalpha=dalpha_aux;
         melhor.cor=cor;
         melhor.Simb=Simb;
%          fprintf('Atualizando sistema\n');
%          disp('c');
%          disp(melhor.c);
%          disp('simb')
%          disp([Simb.y_1 Simb.y_2]);
%          [Simb2,~,~,~,~]=PF(melhor.exp,0,c);
%          disp('simb verificado')
%          disp([Simb2.y_1 Simb2.y_2]);
     end
     
end

fprintf('\n\n Best \n');
fprintf('SEA = %f  Alpha-distance = %f\n\n',melhor.SEA,melhor.dalpha);
c=melhor.c;
model.exp=melhor.exp;
model.x=melhor.x;
model.s=atualizaSx(model.s,model.x); 
% fprintf('Sistema Retornado OtimizaSEA\n');
%          disp('c');
%          disp(c);
%          [Simb2,~,~,~,~]=PF(exp,0);
%          disp('simb verificado')
%          disp([Simb2.y_1 Simb2.y_2]);
end


function [melhor, cor,Simb,SEA,dalpha]=melhorSistema3(sis,model,d,dem,SEAmax,AlphaMin,window,c,dmax,bd)

n=size(d(1).MY,1);

tam =size(sis,2);
disp('Chosing the best model');

VSEA=ones(1,tam)*NaN;
fprintf('Evaluating the model(Total %d) [ ',tam);
for k=1:tam
    x=sis(k).x;
    [SEA,~,~,~]=  sea(model, x,d, dem,window);
    
    if SEA <=SEAmax
        SEAmax=SEA;
        fprintf('%d(B ',k);
        cor=basinAttraction(window,model,x,dmax,n);
        rm=checkRM(d,dem,model,cor,bd,window);        
        if rm==1
            [dalpha,~,~]=calcAlpha(cor,window.mx,window.my,bd,dem);
            fprintf('%.2fda ) ',dalpha);
            if dalpha>AlphaMin
                VSEA(k)=SEA;
                V(k).cor=cor;
                V(k).dalpha=dalpha;
            end
        else
            fprintf(') ');
        end
    else
        fprintf('%d ',k);
    end
     
end

fprintf(']\n\n');
[minSEA,k]=min(VSEA);

if isnan(minSEA)
    melhor=[];
    cor=[];
    Simb=[];
    SEA=NaN;
    dalpha=0;
    
    return;
end

%melhor escolhido em k
melhor=sis(k);
cor=V(k).cor;
[Simb]=PF(sis(k).exp,0,c);
SEA=VSEA(k);
dalpha=V(k).dalpha;
end







