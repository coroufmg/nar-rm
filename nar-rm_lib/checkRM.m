function [rm] =checkRM(d,dem,model,cor,bd,window)
%-----------------
%testar se possui a regi�o de opera��o
rm=1;

[SEA,~,~,~]= sea(model, model.x, d, dem,window);
if isnan(SEA)
    rm=0;
    return;
end

for i=1:dem
    tam=size(bd(i).p,1);
    for j=1:tam
        lx=bd(i).p(j,1);
        ly=bd(i).p(j,2);
        if cor(ly,lx)~=1
            rm=0;
            return;
        end
    end
end
