function [px,py]=cellToPoint(lx,ly,window)
            px=window.mx(1,1)+(lx-1)*window.dx;
            py=window.my(1,1)-(ly-1)*window.dy;
end