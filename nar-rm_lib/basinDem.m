function [bacia,bd]=basinDem(d,dem,mx,my)
%retorna em:
%   * bacia - o mapa com o valor 2 onde tem os dados das demonstra��es
%   * bd - as posi��es (x, y) no mapa onde tem os dados das demonstra��es

dx=mx(1,2)-mx(1,1);
dy=my(1,1)-my(2,1);
bacia=zeros(size(mx));

tam=size(d(1).MY,1);

for i=1:dem
    bd(i).p=zeros(tam,2);
    aux=1;
    for j=1:tam
        px=d(i).MY(j,1);
        py=d(i).MY(j,2);
        lx=round((px-mx(1,1))/dx)+1;
        ly=round((my(1,1)-py)/dy)+1;
        bacia(ly,lx)=2;
        bd(i).p(j,:)=[lx ly];
        p_px=px;
        p_py=py;
        
        %falta verificar a rela��o entre os pontos se tem celula no
        %meio
        if j>1 %segunda itera��o do la�o
            if abs(lx-lxant)==0 && abs(ly-lyant)>1%pulo celula para cima ou baixo
                ini=min(ly,lyant);
                fim=max(ly,lyant);
                for k=ini:fim
                    bacia(k,lx)=2;
                    bd(i).p(tam+aux,:)=[lx k];
                    aux=aux+1;
                end;
            elseif abs(ly-lyant)==0 && abs(lx-lxant)>1%pulo celula para os lados
                ini=min(lx,lxant);
                fim=max(lx,lxant);
                for k=ini:fim
                    bacia(ly,k)=2;
                    bd(i).p(tam+aux,:)=[k ly];
                    aux=aux+1;
                end;
            elseif abs(lx-lxant)>1 || abs(ly-lyant)>1
                px=[p_px p_pxant];
                py=[p_py p_pyant];
                X=px(1):(px(2)-px(1))/4:px(2);
                Y=interp1(px,py,X);
                for k=1:size(X,2)
                    llx=round((X(k)-mx(1,1))/dx)+1;
                    lly=round((my(1,1)-Y(k))/dy)+1;
                    bacia(lly,llx)=2;
                    bd(i).p(tam+aux,:)=[llx lly];
                    aux=aux+1;
                end;
            end;
        end;
        
        lxant=lx;
        lyant=ly;
        p_pxant=p_px;
        p_pyant=p_py;
    end
end;

end
