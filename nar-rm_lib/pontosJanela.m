function np= pontosJanela(Simb,mx,my)
dx=mx(1,2)-mx(1,1);
dy=my(1,1)-my(2,1);
nx=size(mx,1);
ny=size(mx,2);

tam=size(Simb.y_1,1);
np=0;
for i=1:tam
    px=double(Simb.y_1(i));
    py=double(Simb.y_2(i));
    lx=round((px-mx(1,1))/dx)+1;
    ly=round((my(1,1)-py)/dy)+1;
    if lx>0 && ly>0 && lx<=nx && ly<=ny
        np=np+1;
    end
end
end