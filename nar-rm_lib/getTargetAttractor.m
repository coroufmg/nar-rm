function [c,model]=getTargetAttractor(model,window,npontos,fat,d,baciadem,c) 
%A fun��o desloca o ponto fixo do alvo com o objetivo de obter um sistema que possua o alvo como atrator 

alvo=target(model.exp);

if alvo ==1
    return;
end

disp('Target is [NOT] an attractor.');
disp('\n\n ||| Performing the function getTargetAttractor() ||| \n\n');


%desloca o ponto fixo do alvo nas npontos posi��es que formam a circufer�ncia de raio
%igual a dist*fat e retorna em sis os sistemas que possuem o alvo atrator e
%que possuem a vari�ncia dos erros menores que maxvarx e maxvary
sis=imposeFP(model,window,npontos,1,0,0,0,window.dist*fat,c,baciadem);

while size(sis,2)==0 && fat>1
    fat=fat-1;
    fprintf('\nNo model found, reducing distance from %f to %f\n',dist*(fat+1),dist*fat);
    sis=imposeFP(model,window,npontos,0,0,0,0,window.dist*fat,c,baciadem);
                         
end


if size(sis,2)==0 
    disp('Could not find a model with the attractor target.');
    return;
end

%escolhe o melhor entre os poss�veis modelos obtidos 
dem=size(d,2);
sis_melhor=melhor(sis,model,d,dem,window);

model.exp=sis_melhor.exp;
c=sis_melhor.c;
model.x=sis_melhor.x;
model.s=atualizaSx(model.s,model.x);
fprintf('Fixed point chosen ( %f , %f ) \n\n',c(1,1),c(1,2));

end

function sis_melhor=melhor(sis,model,d,dem,window)
%Esta fun��o avalia os melhores sistemas em sis usando os seguintes crit�rios
%  1 - O melhor SEA
%  2 - O melhor SEAcomp. O SEAcomp � calculado acumulando o SEA das
%      demonstra��es que n�o convergem para o infinito (SEAp), adicionado
%      10000 a cada demonstra��o que converge para o infinito.

tam =size(sis,2);
disp('Chosing the best model');

V=zeros(1,tam);
Vnsea=zeros(1,tam);
fprintf('Evaluating the model (Total %d) [ ',tam);
for k=1:tam
    x=sis(k).x;
    [~,~,nsea,SEAp]= sea(model, x,d, dem,window);
    V(k)=SEAp;
    Vnsea(k)=nsea;
    fprintf('%d ',k); 
end

fprintf(']\n\n');
[maxnsea,k]=max(Vnsea);

minSEAp=V(k);

for i=1:tam
    if maxnsea==Vnsea(i) && minSEAp>V(i)
     minSEAp=V(i);
     k=i;
    end
end
sis_melhor=sis(k); 
end