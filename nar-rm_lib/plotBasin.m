function plotBasin(ng,cor,window,color)
figure(ng);
hold on;
dx=window.dx;
dy=window.dy;
for j=2:window.ny-1 %sai de 2 para n�o colorir as bordas
    ok=0;
    for k=2:window.nx-1
        if cor(j,k)==1
            if ok==0
                pxini=window.mx(j,k);
                ok=1;
            end;
        else
            if ok==1
                px=window.mx(j,k-1);
                py=window.my(j,k-1);
                npx=px-dx/2;
                npy=py-dy/2;
                npxini=pxini-dx/2;
                xinicio=max(npxini,window.minx+dx);
                xfim=min(npx+dx,window.maxx-dx);
                yinicio=max(npy,window.miny+dy);
                yfim=min(npy+dy,window.maxy-dy);
                h1=fill([xinicio, xinicio, xfim, xfim, xinicio ],[yinicio, yfim, yfim, yinicio, yinicio],color(1,:));
                set(h1,'EdgeColor',color(1,:));
            end;
            
            ok=0;
        end;
        
    end;
    k=window.nx;
    if ok==1
        px=window.mx(j,k-1);
        py=window.my(j,k-1);
        npx=px-dx/2;
        npy=py-dy/2;
        npxini=pxini-dx/2;
        xinicio=max(npxini,window.minx+dx);
        xfim=min(npx+dx,window.maxx-dx);
        yinicio=max(npy,window.miny+dy);
        yfim=min(npy+dy,window.maxy-dy);
        h1=fill([xinicio, xinicio, xfim, xfim, xinicio ],[yinicio, yfim, yfim, yinicio, yinicio],color(1,:));
        set(h1,'EdgeColor',color(1,:));
    end;
    
end;
axis([window.minx window.maxx window.miny window.maxy]);
box on;
hold off;
end