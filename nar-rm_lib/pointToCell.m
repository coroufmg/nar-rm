function [lx,ly]=pointToCell(px,py,window)
lx=round((px-window.mx(1,1))/window.dx)+1;
ly=round((window.my(1,1)-py)/window.dy)+1;
end