function v_dizimado = dizima(v_original,dec_factor)
% function v_dizimado = dizima(v_original,dec_factor) dizima vetores
%
%	entrada
%	  v_original	- vetor original
%	  dec_factor	- fator de dizimacao
%
%	saida
%	  v_dizimado	- vetor dizimado resultante
%

% Eduardo Saraiva - 08/03/97
% CPDEE - UFMG 
  
[a,b] = size(v_original);
i=1;
j=1;

while j<=a,
	v_dizimado(i,:) = v_original(j,:);
	j = j + dec_factor;
	i = i + 1;
end;
