function plotExecution(name,model,d,dt,ng,window)
f=figure(ng);
clf;
dem=size(d,2);
n=size(d(1).MY,1);

L=plotDem(0,d,dem,ng,'',window.minx, window.maxx, window.miny, window.maxy);
plotVectorField(model,dt,ng,window);

hold on;
for i=1:dem
    ym=msimodel_target(model.model,model.x,model.nnt,d(i).MY(1,:),n,window);
    L(4)=plot(ym(:,1),ym(:,2),'k','LineWidth',2);
end
L(3)=plot(0,0,'k*','MarkerSize',11,'LineWidth',2);
legend(L,{'Training Data'; 'Dynamic Flow'; 'Target';'Reproduction' },'Orientation','horizontal','Location','northoutside');
hold off;

set(f,'Name',name);
pause(0.1);
end