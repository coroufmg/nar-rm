function [pnb]=baciaNaoDem(bd,cor)
%retorna em pnb as c�lulas que possuem os dados das demonstra��es que n�o
%pertencem � bacia de atra��o do alvo
dem=size(bd,2);

pnb=[];
for i=1:dem
    tam=size(bd(i).p,1);
    for j=1:tam
        lx=bd(i).p(j,1);
        ly=bd(i).p(j,2);
        if cor(ly,lx)==0
            pnb=[pnb; lx ly];
        end
    end
end