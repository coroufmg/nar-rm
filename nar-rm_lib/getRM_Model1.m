function [c,model]=getRM_Model1(model,window,npontos,fat,d,baciadem,c,MaxIt)
%Esta fun��o tenta obter a regi�o de opera��o deslocando pontos fixos fora
%da bacia de atra��o e utilizando como avalia��o o valor do SEA.

dem=size(d,2);
[SEA,~,nsea,~]= sea(model, model.x, d, dem,window);

if nsea==dem
   disp('All starting points reach the target.');
   return; 
end

[Simb,~,tpf,~,~]=PF(model.exp,0,c);

disp('Some starting points [NOT] reach the target.');
fprintf('\n\n ||| Performing the function getRM_Model1 ||| \n\n');


melhor.c=c;
melhor.exp=model.exp;
melhor.x=model.x;
melhor.SEA=SEA;
melhor.Simb=Simb;
melhor.tpf=tpf;



%fprintf('pnb (numero de celulas das demonstra��es que n�o eprtencem a bacia) = %d',pnb);
fprintf('\n[ Distance used to impose fixed points dist = %f] \n\n',window.dist*fat);

% ---------- Exce��o Sistemas que s� possuem um �nico ponto fixo ---------
% Neste caso pontos fixos s�o adicionados pr�ximos �s bordas, na tentativa
% de adicionar pontos que permitam aplicar o m�todo para obter a RO
if size(Simb.y_1,1)==1
    sis=sistemaUmPF(model,window,window.dist*fat,c,baciadem,MaxIt);
    if size(sis,2)==0
        disp('No model found.');
        return;
    end
    
    [sis, Simb]=melhorSistema2(sis,model,d,dem,window,c);
    c=sis.c;
    model.exp=sis.exp;
    model.x=sis.x;
    model.s=atualizaSx(model.s,model.x);
    [SEA,~,nsea,~]= sea(model, model.x,d, dem,window);
    fprintf('[   [SEA = %f  nsea = %d]    ]\n\n',SEA,nsea);
    if nsea==dem 
        disp('All starting points reach the target.');
        fprintf('Fixed point chosen ( %f , %f ) \n\n',c(1,1),c(1,2));
        return;
    end
end
%--------------------------------------------------------------------------






it=0;

while it<MaxIt %enquanto n�o tiver RO ou atingir o valor m�ximo de itera��es
   
    %--------------------------------------------------------------------------
    %Definindo os par�metros utilizados na fun��o deslocaPontoCirc
    op=1; %Pontos dentro da janela
    op2=0; % Fora Bacia
    np = pontosJanela(Simb,window.mx,window.my); %obtem o n�mero de pontos fixos dentro da janela
    if np==1 %so tem um ponto fixo dentro da janela.
        op=0; %Pontos dentro e fora da janela
        op2=0; % Fora Bacia
    end
    %--------------------------------------------------------------------------
    it=it+1;
    fprintf('iteration %d [MAXit=%d]\n',it,MaxIt);
    tam=size(Simb.y_1,1);
    k=0;
    vsis=[];
    fprintf('\nImposing fixed points' );
    for i=1:tam
%         if tpf(i)~=3
%             continue;
%         end
        px=double(Simb.y_1(i));
        py=double(Simb.y_2(i));
        if(px~=0 || py~=0) %ponto fixo n�o � o alvo
            k=k+1;
            fprintf('( %.2f, %.2f ) ',px,py);
            sis=imposeFP(model,window,npontos,op,op2,px,py,window.dist*fat,c,baciadem);
                                 
            fprintf('[%d]',size(sis,2));
            if size(sis,2)==0
                k=k-1;
            else
                vsis=[vsis sis];
            end
            
        end
    end
    fprintf('\n');
    
    if(k==0)
        fprintf('\nNo model found\n');
        if fat>1
%             fat=fat-1;
            fat=fat-2;
            fprintf('Reducing distance from %f to %f\n',dist*(fat+1),dist*fat',dist*(fat+1),dist*fat);
            disp('Coming back to the best model found.');
            %Atualizando para o melhor sistema obtido at� o momento
            c=melhor.c;
            model.exp=melhor.exp;
            model.x=melhor.x;
            SEA=melhor.SEA;
            Simb=melhor.Simb;
            model.s=atualizaSx(model.s,model.x);
            tpf=melhor.tpf;
            
            continue;
        else
            break;
        end;
    end
    
    [sis,Simb,tpf]=melhorSistema2(vsis,model,d,dem,window,c);
    
    
    c=sis.c;
    disp('Fixed point chosen');
    disp(c);
    model.exp=sis.exp;
    model.x=sis.x;
    model.s=atualizaSx(model.s,model.x);
    
    [SEA,~,nsea,~]= sea(model, model.x,d, dem,window);
    fprintf('[   [SEA = %f nsea = %d]    ]\n\n',SEA,nsea);
    if nsea==dem %encontrou RO
        disp('All starting points reach the target.');
        fprintf('Model found with Fixed point [ ');
        for i=1:size(c,1);
            fprintf('(%f, %f) ',c(i,1),c(i,2));
        end
        fprintf(']\n');
         melhor.c=c;
         melhor.exp=model.exp;
         melhor.x=model.x;
         melhor.SEA=SEA;
         melhor.Simb=Simb;
         melhor.tpf=tpf;
        break;
    end
    
     %gravando melhor
     if SEA<melhor.SEA
         melhor.c=c;
         melhor.exp=model.exp;
         melhor.x=model.x;
         melhor.SEA=SEA;
         melhor.Simb=Simb;
         melhor.tpf=tpf;
     end
    
end


    c=melhor.c;
    model.exp=melhor.exp;
    model.x=melhor.x;
    model.s=atualizaSx(model.s,model.x);
 
end



function sis=sistemaUmPF(model,window,dist,c,baciadem,MaxIt)
fprintf('Model with a single fixed point.\n Adding other points to apply the method\n');
it=0;
fat=1;
cont=1;
sis=[];
dist_ini=dist;
while it<MaxIt
    it=it+1;
    fprintf('iteration %d [MAXit=%d]\n',it,MAXit);
    %Definindo os par�metros utilizados na fun��o deslocaPontoCirc
    op=0; %Qualquer lugar
    op2=0; % fora da bacia
    distancia=min(window.dx,window.dy)*((size(window.mx,1)-2)/2);
    
    %distancia=dist*size(mx,1)/2; %Definindo o raio da circufer�ncia com a metade da distancia da janela em rela��o ao eixo x
    sis=imposeFP(model,window,16,op,op2,0,0,distancia,c,baciadem);
    k=size(sis,2);
    if(k==0)
        fat=fat+1;
        fprintf('\nNo model found, reducing distance from %f to %f\n',dist,dist_ini*fat);
        dist=dist_ini*fat;
        cont=cont+1;
        continue;
    end
    return;
end
end

function [melhor, Simb, tpf]=melhorSistema2(sis,model,d,dem,window,c)
%Esta fun��o avalia os melhores sistemas em sis usando os seguintes crit�rios
%  1 - O melhor SEA
%  2 - O melhor SEAcomp. O SEAcomp � calculado acumulando o SEA das
%      demonstra��es que n�o convergem para o infinito (SEAp), adicionado
%      10000 a cada demonstra��o que converge para o infinito.

tam =size(sis,2);
disp('Chosing the best model');

V=zeros(1,tam);
Vnsea=zeros(1,tam);
fprintf('Evaluating the model (Total %d) [ ',tam);
for k=1:tam
    x=sis(k).x;
    [~,~,nsea,SEAp]= sea(model, x,d, dem,window);
    V(k)=SEAp;
    Vnsea(k)=nsea;
    fprintf('%d ',k); 
end

fprintf(']\n\n');
[maxnsea,k]=max(Vnsea);

minSEAp=V(k);

for i=1:tam
    if maxnsea==Vnsea(i) && minSEAp>V(i)
     minSEAp=V(i);
     k=i;
    end
end

exp=sis(k).exp;
[Simb,~,tpf,~,~]=PF(exp,0,c);
melhor=sis(k);

end




