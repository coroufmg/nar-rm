function [xd]=plotVectorField(model,dt,ng,window)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
figure(ng);
hold on;

mat=[window.mx(:) window.my(:)]';
xd=zeros(size(mat));
%xp=zeros(size(mat));
for i=1:size(mat,2)
    g=ones(1,2);
    g(:,1)=g(:,1)*mat(1,i);
    g(:,2)=g(:,2)*mat(2,i);
    resp=msimodel(model.model,model.x(:,1),model.nnt,g,[],[],2);
    dresp=diff(resp)/dt;
    xd(:,i)=[dresp(1,1); dresp(1,2)];
end;
streamslice(window.mx,window.my,reshape(xd(1,:),window.ny,window.nx),reshape(xd(2,:),window.ny,window.nx),1,'method','cubic');
axis([window.minx window.maxx window.miny window.maxy]);
box on;
hold off;
end

