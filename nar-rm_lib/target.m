function op=target(exp)
%Avaliando os pontos fixos
rf(1)=sym(exp(1).simb(6:end));
rf(2)=sym(exp(2).simb(6:end));
syms y_1 y_2;
varia=[y_1; y_2];
jacob=jacobian(rf,varia);
%verificando o alvo
y_1=0;
y_2=0;
av=double(eig(eval(jacob)));
av(1)=abs(av(1));
av(2)=abs(av(2));

if av(1)<1 && av(2)<1
    op=1;
else
    op=0;
end;
end