function plotAll(name,model,d,dt,ng,window,c,dmax,bd)
%plota o plano de fases contendo os pontos fixos, a integral do campo
%vetorial e a bacia de atra��o do alvo (caso ela exista) 
disp('Plotting de model');
dem=size(d,2);

f=figure(ng);
clf;

n=size(d(1).MY,1);

[Simb,~,tpf,~,alvo]=PF(model.exp,1,c);
[SEA,~,~,~]= sea(model, model.x, d, dem,window);
fprintf('SEA = %f\n',SEA);


if alvo ~=0
     cor=basinAttraction(window,model,model.x,dmax,n);                 
else
    cor=[];
    %name=[name ' [Alvo n�o � atrator] [Sem regi�o de opera��o]'];
end

rm=0;
if alvo ==1
    plotBasin(ng,cor,window,[190/255 255/255 135/255]);
    rm=checkRM(d,dem,model,cor,bd,window);
%     if rm==0
%         name=[name ' [Alvo � atrator] [Sem regi�o de opera��o]'];
%     end
end
L=plotDem(1,d,dem,ng,'',window.minx, window.maxx, window.miny, window.maxy);
plotVectorField(model,dt,ng,window);
plotFixedPoint(Simb,tpf,ng,L,'o');

if rm==1
    [alpha,p1,p2]=calcAlpha(cor,window.mx,window.my,bd,dem);
    fprintf('\n Alpha-distance = %f \n',alpha);
    plotAlpha(ng,p1,p2,window.minx, window.maxx, window.miny, window.maxy);
    xlabel(['$x$, $sea$=' num2str(SEA,'%.2f') ', $rm$=yes, $\alpha$=' num2str(alpha,'%.2f')],'interpreter','latex','fontsize',15);
    %name=[name ' [Alvo � atrator] [Com regi�o de opera��o]'];
else
    xlabel(['$x$, $sea$=' num2str(SEA,'%.2f') ', $rm$=no'],'interpreter','latex','fontsize',15);
end

set(f,'Name',name);
pause(0.1);
end
