function [SEA,vsea,nsea,SEAp] = sea(model, x, d, dem,window)
%Esta fun��o calcula um SEA diferente, pois adiciona o desvio padr�o de cada dmonstra��o ao valor do SEA.
%A ideia � valorizar sistemas com menor desvio padr�o entre as demonstra��es.
%Esta fun��o retorna:
%  -SEA : o valor do SEA calculado
%  -vsea: o valor do SEA para cada demonstra��o
%  -nsea: o n�mero de demonstra��es que n�o convergem para o infinito 
%  -SEAp: o valor do SEA para as demonstra��es que n�o convergem para o infinito

[n,~]=size(d(1).MY);

A=0; 
nsea=dem;
vsea=zeros(1,dem);

for i=1:dem
    ym=msimodel_target(model.model,x,model.nnt,d(i).MY(1,:),n,window);
    tam=size(ym,1);
    for j=1:tam-1
        if(sum(isnan([ym(j,1),ym(j+1,1),ym(j,2),ym(j+1,2)]))>0)
            A=NaN;
            vsea(i)=NaN;
            nsea=nsea-1;
            break;
        end;
        if j<=n-1
            aux=areaquadrilatero([ym(j,1),ym(j+1,1),d(i).MY(j,1),d(i).MY(j+1,1)],[ym(j,2),ym(j+1,2),d(i).MY(j,2),d(i).MY(j+1,2)]);
        else
            aux=areaquadrilatero([ym(j,1),ym(j+1,1),0,0],[ym(j,2),ym(j+1,2),0,0]);
        end;
        A = A + aux;
        vsea(i)=vsea(i)+aux;
    end
end;

SEA=A/dem;

SEAp=0;
cont=0;
for i=1:dem
    if ~isnan(vsea(i))
        cont=cont+1;
        SEAp=SEAp+vsea(i);
    end
end

if cont==0
    SEAp=NaN;
else
    SEAp=SEAp/cont;
end
end

