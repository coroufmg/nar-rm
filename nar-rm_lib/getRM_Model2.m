function [c,model]=getRM_Model2(model,window,npontos,fat,d,baciadem,bd,c,MaxIt,dmax)
%Esta fun��o tenta obter a regi�o de opera��o deslocando pontos fixos fora
%da bacia de atra��o e utilizando como avalia��o o n�mero de c�lulas que
%possuem dados das demonstra��es e que n�o pertencem � bacia.

dist=window.dist;
dem=size(d,2);
[Simb,~,tpf,~,~]=PF(model.exp,0,c);
[SEA,~,~,~]= sea(model, model.x, d, dem,window);

n=size(d(1).MY,1);

cor=basinAttraction(window,model,model.x,dmax,n); 
rm=checkRM(d,dem,model,cor,bd,window);



if rm == 1 
    disp('RM model');
    return;
end

rm=0;
disp('Is [NOT] RM model');
fprintf('\n\n ||| Performing the function getRM_Model2 ||| \n\n');

pnb=baciaNaoDem(bd,cor); %retorna as celulas que possuem dados das demonstra��es que n�o pertencem � bacia
pnb=size(pnb,1);

melhor.c=c;
melhor.exp=model.exp;
melhor.x=model.x;
melhor.pnb=pnb;
melhor.SEA=SEA;
melhor.Simb=Simb;
melhor.tpf=tpf;
melhor.cor=cor;


fprintf('pnb (number of cells that have points of the demonstrations that do not belong to the basin of attraction) = %d',pnb);
fprintf('\n[ Distance used to impose fixed points dist = %f] \n\n',window.dist*fat);

% ---------- Exce��o Sistemas que s� possuem um �nico ponto fixo ---------
% Neste caso pontos fixos s�o adicionados pr�ximos �s bordas, na tentativa
% de adicionar pontos que permitam aplicar o m�todo para obter a RO
if size(Simb.y_1,1)==1
    sis=sistemaUmPF(model,window,window.dist*fat,c,baciadem,MaxIt);
    if size(sis,2)==0
        disp('No model found.');
        return;
    end
    
    [sis, cor, pnb, Simb]=melhorSistema(sis,model,d,dem,bd,window,c,dmax);                  
    c=sis.c;
    exp=sis.exp;
    x=sis.x;
    s=atualizaSx(s,x);
    [SEA,~,~,~]= seaDP(model, x, nnt,d,n, dem,janela);
    fprintf('[   [SEA = %f  pnb = %d]    ]\n\n',SEA,pnb);
    if pnb==0 && ~isnan(SEA) %encontrou RO
        rm=1;
        disp('RM model found');
        fprintf('Fixed point chosen ( %f , %f )\n\n',c(1,1),c(1,2));
        return;
    end
end




it=0;
while it<MaxIt %enquanto n�o tiver RO ou atingir o n�mero m�ximo de itera��es
    %--------------------------------------------------------------------------
    %Definindo os par�metros utilizados na fun��o deslocaPontoCirc
    np = pontosJanela(Simb,window.mx,window.my);
    op=1; %Pontos dentro da janela
    op2=0; % Fora da Bacia
    if np==1 %so tem um ponto fixo dentro da janela.
        op=0; %Pontos dentro da janela
        op2=0; % Fora Bacia
    end
    %--------------------------------------------------------------------------

    it=it+1;
    fprintf('iteration %d [MAXit=%d]\n',it,MaxIt);
    tam=size(Simb.y_1,1);
    k=0;
    vsis=[];
    fprintf('\nImposing fixed points ' );
%     [ns,~]=nsaddles(Simb,mx,my,tpf);
    for i=1:tam
%         if ns>0 && tpf(i)~=3
%         if tpf(i)~=3
%             continue;
%         end
        px=double(Simb.y_1(i));
        py=double(Simb.y_2(i));
        if(px~=0 || py~=0) %o ponto fixo n�o � o alvo
            k=k+1;
            fprintf('( %.2f, %.2f ) ',px,py);
            sis=imposeFP(model,window,npontos,op,op2,px,py,window.dist*fat,c,baciadem,cor);
            fprintf('[%d]',size(sis,2));
            if size(sis,2)==0
                k=k-1;
            else
                vsis=[vsis sis];
            end
        end
    end
    fprintf('\n');
    
    if(k==0)
        disp('\nNo model found\n');
        if fat>1
%             fat=fat-1;
            fat=fat-2;
            fprintf('Reducing distance from %f to %f\n',dist*(fat+1),dist*fat);
            disp('Coming back to the best model found.');
            %Atualizando para o melhor sistema obtido at� o momento
            c=melhor.c;
            model.exp=melhor.exp;
            model.x=melhor.x;
            pnb=melhor.pnb;
            SEA=melhor.SEA;
            Simb=melhor.Simb;
            tpf=melhor.tpf;
            cor=melhor.cor;
            model.s=atualizaSx(model.s,model.x);
            
            continue;
        else
            break;
        end;
    end
    
    [sis, cor, pnb,Simb,tpf]=melhorSistema(vsis,model,d,dem,bd,window,c,dmax); %melhor sistema de todos os pontos
    
     x=sis.x;
    [SEA,~,~,~]= sea(model, x,d, dem,window);
       
    if isnan(SEA)
         fprintf('No model found\n\n');
         if fat>1
%             fat=fat-1;
            fat=fat-2;
            fprintf('Reducing distance from %f to %f\n',dist*(fat+1),dist*fat);
            disp('Coming back to the best model found.');
            %Atualizando para o melhor sistema obtido at� o momento
            c=melhor.c;
            model.exp=melhor.exp;
            model.x=melhor.x;
            pnb=melhor.pnb;
            SEA=melhor.SEA;
            Simb=melhor.Simb;
            tpf=melhor.tpf;
            cor=melhor.cor;
            model.s=atualizaSx(model.s,model.x);
            
            continue;
        else
            break;
        end;
     end
    
    
        
    c=sis.c;
    disp('Fixed point chosen');
     disp(c);
    model.exp=sis.exp;
    model.x=sis.x;
    model.s=atualizaSx(model.s,model.x);
    
    %[SEA,~,~,~]= seaDP(model, x, nnt,d,n, dem,janela);
    fprintf('[   [SEA = %f  pnb = %d]    ]\n\n',SEA,pnb);
    %fprintf('[   [SEA = %f pnb = %d]    ]\n\n',SEA,pnb);
    if pnb==0 && ~isnan(SEA) %encontrou RO
        rm=1;
        disp('RM model found');
        fprintf('Model found with Fixed point [ ');
        for i=1:size(c,1);
            fprintf('(%f, %f) ',c(i,1),c(i,2));
        end
        fprintf(']\n');
%         fprintf('] varx = %f vary = %f\n\n',sis.varx,sis.vary);
        melhor.c=c;
        melhor.exp=model.exp;
        melhor.x=model.x;
        melhor.pnb=pnb;
        melhor.SEA=SEA;
        melhor.Simb=Simb;
        melhor.tpf=tpf;
        melhor.cor=cor;
        break;
    end
    
    %gravando melhor
    if pnb<melhor.pnb
        melhor.c=c;
        melhor.exp=model.exp;
        melhor.x=model.x;
        melhor.pnb=pnb;
        melhor.SEA=SEA;
        melhor.Simb=Simb;
        melhor.tpf=tpf;
        melhor.cor=cor;
    elseif pnb==melhor.pnb && SEA <melhor.SEA
        melhor.c=c;
        melhor.exp=model.exp;
        melhor.x=model.x;
        melhor.pnb=pnb;
        melhor.SEA=SEA;
        melhor.Simb=Simb;
        melhor.tpf=tpf;
        melhor.cor=cor;
    end
    
end

%if ro==0
    %Atualizando para o melhor sistema obtido at� o momento
    c=melhor.c;
    model.exp=melhor.exp;
    model.x=melhor.x;
    %pnb=melhor.pnb;
    %SEA=melhor.SEA;
    %Simb=melhor.Simb;
    %cor=melhor.cor;
    model.s=atualizaSx(model.s,model.x);
%end

end


function [melhor, cor, pnb, Simb,tpf]=melhorSistema(sis,model,d,dem,bd,window,c,dmax)
%Esta fun��o avalia os melhores sistemas em sis usando os seguintes crit�rios
%  1 - O menor n�mero de c�lulas que possuem dados das demonstra��es e que n�o pertencem � bacia
%  2 - O melhor SEAcomp. O SEAcomp parcial � calculado acumulando o SEA das
%      demonstra��es que n�o convergem para o infinito (SEAp), adicionado
%      10000 a cada demonstra��o que converge para o infinito.
n=size(d(1).MY,1);

tam =size(sis,2);
disp('Chosing the best model');

VSEA=zeros(1,tam);
Vpnb=zeros(1,tam);
fprintf('Evaluating the model (Total %d) [ ',tam);
for k=1:tam
    x=sis(k).x;
    %fprintf('%d(B) ',k);
    [VSEA(k),~,~,~]=  sea(model, x,d, dem,window);
    
    if ~isnan(VSEA(k))
        cor=basinAttraction(window,model,x,dmax,n); 
        pnb=baciaNaoDem(bd,cor); %retorna as celulas que possuem dados das demonstra��es que n�o pertencem � bacia
        Vpnb(k)=size(pnb,1);
        fprintf('%d(B pnb=%d ) ',k,Vpnb(k));
        V(k).cor=cor;
    else
        Vpnb(k)=window.nx*window.ny;
        V(k).cor=[];
        fprintf('%d( pnb=%d) ',k,Vpnb(k));
    end
end
fprintf(']\n\n');

[minpnb,k]=min(Vpnb); %Procura o que tem o menor valor pnb
minSEA=VSEA(k);

for i=1:tam %buscando o menor SEA entre os de menor pnb
    
    if Vpnb(i)==minpnb
%         fprintf(' %f ', VSEAcomp(i));
        if VSEA(i)<minSEA
            minSEA=VSEA(i);
            k=i;
        end
    end
end
% fprintf('\n');


%melhor escolhido em k
melhor=sis(k);
cor=V(k).cor;
pnb=Vpnb(k);
[Simb,~,tpf,~,~]=PF(sis(k).exp,0,c);

end


function sis=sistemaUmPF(model,window,dist,c,baciadem,MaxIt)
fprintf('Model with a single fixed point.\n Adding other points to apply the method\n');
it=0;
fat=1;
cont=1;
sis=[];
dist_ini=dist;
while it<MaxIt
    it=it+1;
    fprintf('iteration %d  %d [MAXit=%d]\n',it,MAXit);
    %Definindo os par�metros utilizados na fun��o deslocaPontoCirc
    op=0; %Qualquer lugar
    op2=0; % fora da bacia
    distancia=min(window.dx,window.dy)*((size(window.mx,1)-2)/2);
    
    %distancia=dist*size(mx,1)/2; %Definindo o raio da circufer�ncia com a metade da distancia da janela em rela��o ao eixo x
    sis=imposeFP(model,window,16,op,op2,0,0,distancia,c,baciadem);
    k=size(sis,2);
    if(k==0)
        fat=fat+1;
        fprintf('\nNo model found, reducing distance from %f to %f\n',dist,dist_ini*fat);
        dist=dist_ini*fat;
        cont=cont+1;
        continue;
    end
    return;
end
end

