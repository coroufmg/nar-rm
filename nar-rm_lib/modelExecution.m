function modelExecution(ng,model,dmax,n)
figure(ng);
hold on;
while(true)
    [px, py, b]=ginput(1);
    g(:,1)=px;
    g(:,2)=py;
    
    if(b==3 || b==2)
        break;
    end;
    for k=1:n
        ym=msimodel(model.model,model.x,model.nnt,g,[],[],2);
        d2p=sqrt( (ym(2,1)-ym(1,1))^2 +  (ym(2,2)-ym(1,2))^2 );
        if d2p> dmax
           ang=atan2(ym(2,2)-ym(1,2),ym(2,1)-ym(1,1));
           ym(2,1)=ym(1,1)+cos(ang)*(dmax);
           ym(2,2)=ym(1,2)+sin(ang)*(dmax);
        end
        plot(ym(:,1),ym(:,2),'k','Linewidth',3);
        g=ym(2,:);
    end
end;

end

