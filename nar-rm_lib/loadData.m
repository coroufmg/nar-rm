function [d, dt, name, dmax]=loadData(dec_factor,dem)
% Entrada:
%     dec_factor : Taxa de decima��o
%     nd: numero de demonstra��es que ser�o carregadas
%
% Saida:
%      dados(i): vetor contendo todas as i demonstra��es
%                O vetor � dividido em x, y, vx, vy
%     dt: tempo m�dio entre os pontos


%Montando os dados
names = {'Angle','BendedLine','CShape'};

n=-1;
while n<0 || n>3
    fprintf('\nAvailable Models:\n')
    fprintf('1 - Angle\n2 - BendedLine\n3 - CShape\n');
    n = input('\nType the number of the model: ');
    if n<0 || n>3
        disp('Wrong model number!')
    end
end

fprintf('Loading model...\n');
load(['DataSet/' names{n}],'demos','dt') %loading the model
name=names{n};
fprintf('dt=%f\n',dt);

for i=1:dem
    dados(i).x=demos{i}.pos(1,:)';
    dados(i).y=demos{i}.pos(2,:)';
end;

if(dec_factor>1)
    fprintf('Decimating data [ decRate = %d ] ...\n',dec_factor);
    for i=1:dem
        dados(i).x=dizima(dados(i).x,dec_factor);
        dados(i).y=dizima(dados(i).y,dec_factor);
        dados(i).x(end+1)=0;
        dados(i).y(end+1)=0;
    end;
    dt=dt*dec_factor;
    fprintf('Correcting dt=%f\n',dt);
    
end;

    %calcula deslocamento max min e m�dio
    dmax=0;
    tam=size(dados(1).x,1);
    for i=1:dem
        for j=1:tam-1
            p1=[dados(i).x(j) dados(i).y(j)];
            p2=[dados(i).x(j+1) dados(i).y(j+1)];
            d2p=sqrt( ( p1(1)-p2(1) )^2 +  ( p1(2)-p2(2) )^2 );
            if d2p>dmax
                dmax=d2p;
            end
        end        
    end
    
    
    dmax=dmax*2;

for i=1:dem
    d(i).MY= [dados(i).x dados(i).y];
end
disp('Finished.');
end

