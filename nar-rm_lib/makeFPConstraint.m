function S = makeFPConstraint(model,xf,yf)

[nt,n2]=size(model);


[npr,nno]=mget_inf(model);

nte=npr+nno;

S=[];

for i=1:nte
    %aux=model(i,(j-1)*n2+1:n2*j);
    kk=floor(model(i,:)/1000);
    kk1=floor((model(i,:)-kk*1000)/100); % subsystem
    kk2=floor((model(i,:)-kk*1000-kk1*100)); % lag
    raf=0;
    for k=1:n2
        if kk(k) == 1
            if raf==0
                if kk1(k)==1
                    b=xf;
                else
                    b=yf;
                end;
                raf=1;
            else
                if kk1(k)==1
                    b=b*xf;
                else
                    b=b*yf;
                end;
                
            end;
        elseif kk(k) == 0
            break;
        end;
    end;
    S=[S b];
    
end;
