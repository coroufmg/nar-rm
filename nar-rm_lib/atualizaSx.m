function s=atualizaSx(s,x)
tam=size(s(1).model,1);
s(1).x=x(1:tam);
s(2).x=x(tam+1:end);
end