function [model,TotTerms] = mgenterm(DG,nys,lagy,nus,lagu,nes,lage)
% function [model,TotTerms] = mgenterm(DG,nys,lagy,nus,lagu,nes,lage)
%          returns the code for MIMO models
%
% On entry
%	DG	- degree of nonlinearity
%	nys	- number of output signals
%	lagy	- maximum lag of the output signal
%	nus	- number of input signals
%	lagu	- maximum lag of the input signal
%	nes	- number of noise signals
%	lage	- maximum lag of the noise signal
%
% On return
%	model	- code used to represent the model
%	TotTerms- number of terms into the model

% Eduardo Mendes - 11/08/94 - Carlos Fonseca
% ACSE - Sheffield

% Eduardo Saraiva - 16/02/98
% CPDEE - UFMG 

if ((nargin < 3) | (nargin > 7) | (nargin == 4) | (nargin == 6))
	error('mgenterm requires 3, 5 or 7 input arguments.');
elseif nargin == 3
	%nus=[];
	%lagu=[];
	%nes=[];
	%lage=[];
    %Modificado por Rafael 18/05/2015
    nus=0;
	lagu=0;
	nes=0;
	lage=0;
    
elseif nargin == 5
	%lage=[];
	%nes=[];
    %Modificado por Rafael 18/05/2015
    lage=0;
    nes=0;
else   if (nes == 0) & (nes ~= lage)
           error('if nes=0 then you have to set lage=0.');  
       end;	
       if (nes ~= nys) & (~((nes == 0) & (lage == 0)))
          error('you have to set nes=nys.');  
       end;
end;

% Tests

[a,b]=size(DG);

if ((a*b) ~= 1)
	error('DG is a scalar');
end;

if (DG < 1)
	error('DG is greater than zero.');
end;

if (nus == 0) & (nus ~= lagu)
    error('if the system is autonomous then you have to set nus=0 and lagu=0.');  
end;

if ((nys ~= 0) & (lagy == 0))
   error('lagy must be different of zero');
end;

if ((nus ~= 0) & (lagu == 0))
   error('lagu must be different of zero');
end;

if ((nes ~= 0) & (lage == 0))
   error('lage must be different of zero');
end;

[a,b]=size(lagy);

if ((a*b) ~= 1)
	error('lagy is a scalar.');
end;

if (lagy < 0)
	error('lagy is positive.');
end;

if ~isempty(lagu)
	[a,b]=size(lagu);
	if ((a*b) ~= 1)
		error('lagu is a scalar.');
	end;
	if (lagu < 0)
		error('lagu is positive.');
	end;
end;

if ~isempty(lage)
	[a,b]=size(lage);
	if ((a*b) ~= 1)
		error('lage is a scalar.');
	end;
	if (lage < 0)
		error('lage is positive.');
	end
end;

% Preparing for the actual calculations

LAGSY = lagy * ones(1,nys);
LAGSU = lagu * ones(1,nus);
LAGSE = lage * ones(1,nes);

LAGS=[LAGSY LAGSU LAGSE];

% Number of signals
NS = max(size(LAGS));

ix = cumsum([2 LAGS]);
NFactors = ix(NS+1)-2;
TotTerms = prod(NFactors+1:NFactors+DG) / prod(1:DG);
model = zeros(TotTerms,DG);

% Do linear terms first
i=1;
while i <= NS,

     for j = 1:nys,
          model(ix(i):ix(i+1)-1,1) = 1000 + j*100 + [1:LAGSY(j)]';
          i=i+1;
     end;

     for j = 1:nus,
          model(ix(i):ix(i+1)-1,1) = 2000 + j*100 + [1:LAGSU(j)]';
          i=i+1;
     end;

     for j = 1:nes,
          model(ix(i):ix(i+1)-1,1) = 3000 + j*100 + [1:LAGSE(j)]';
          i=i+1;
     end;
end;

a = NFactors*eye(1,DG);

for i = ix(NS+1):TotTerms,
    % Find term to be incremented next
    [ans,j]=max(a<NFactors);
    a(1:j) = (a(j)+1) * ones(1,j);
    model(i,:) = model(a+1,1)';
end;

[npr,nno,lag,ny,nu,ne,newmodel]=mget_inf(model);

model=newmodel;
