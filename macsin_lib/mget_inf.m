function [npr,nno,lag,ny,nu,ne,newmodel] = mget_inf(model,nt)
% function [npr,nno,lag,ny,nu,ne,newmodel] = mget_inf(model,nt) 
%       gets useful information from the model
%
% On entry      
%       model   - code used for representing a given model
%	nt	- number of terms of each subsistem model
%
% On return
%	npr	- number of process terms
%	nno	- number of noise terms
%	lag	- maximum lag
%	ny	- number of output terms
%	nu	- number of input terms
%	ne	- number of noise terms
%	newmodel- new model where the process terms come first.
%

% Eduardo Mendes - 11/08/94
% ACSE - Sheffield

% Eduardo Saraiva - 16/02/98
% CPDEE - UFMG 

if (( nargin < 1) | ( nargin >2))
	error('mget_inf requires 1 or 2 input arguments.');
end;

[ntt,degree]=size(model);

if (nargin == 1)
   nt = ntt;
end;

nsuby= size(nt,2);

npr=zeros(1,nsuby);nno=zeros(1,nsuby);lag=0;
ny=0;nu=0;ne=0;

pos = ones(1,nsuby);

if nsuby >1
   for i=2:nsuby
       pos(1,i) = pos(1,i-1) + nt(1,i-1);
   end;
end;

for k=1:nsuby
        kkk=1:degree;
	pivp=[];
	pivn=[];
	for i=pos(k):pos(k)+nt(k)-1
		kk=floor(model(i,kkk)/1000); % Signal
		kk1=floor((model(i,kkk)-kk*1000)/100); % Subsystem
		kk2=model(i,kkk)-kk*1000-kk1*100;       % lags
		j=find(kk == 1);
		if ~isempty(j)
%                       ny=ny+1;
			ny=max([ny kk1(j)]);
		end;
		j=find(kk == 2);
		if ~isempty(j)
%                       nu=nu+1;
			nu=max([nu kk1(j)]);
		end;
		j=find(kk == 3);
		if ~isempty(j)
%                       ne=ne+1;
			ne=max([ne kk1(j)]);
			nno(k)=nno(k)+1;
			pivn=[pivn i];
		else
			npr(k)=npr(k)+1;
			pivp=[pivp i];
		end;
		lag=max([lag kk2]);
	end;
        if nsuby == 1
           model(1:length(pivp)+length(pivn),kkk)=[model(pivp,kkk)
                 model(pivn,kkk)];
        end;

end;

newmodel=model;
