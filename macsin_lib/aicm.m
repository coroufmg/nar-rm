function AIC = aicm(covar,v,N,p)
% function AIC = aicm(covar,v,N,p) calculates AIC criterion for MIMO models
%
% Input - covar  - covariance matrix
%         v  - number of model terms
%         N  - number of valid data points
%         p  - parameter (default = 4.0)
%
% Output - AIC(1,nsuby)
%	  where nsuby is the number of subsystem

% Eduardo Mendes - 6/04/93



if nargin == 3
   p = 4.0;
elseif ((nargin < 3) | (nargin > 4))
	error('AICM requires 3 or 4 input arguments.');
end;

[n,nsuby]=size(covar);

[a,b]=size(v);

if (a < b)
	v=v';
end;

if ((a*b) ~= nsuby)
	error('Vector v must have length equal to nsuby.');
end;

[a,b]=size(N);

if (a < b)
	N=N';
end;

if ((a*b) ~= nsuby)
	error('Vector N must have length equal to nsuby.');
end;

[a,b]=size(p);

if ((a*b) ~= 1)
	error('p must be a scalar.');
end;


AIC=zeros(1,nsuby);

for i=1:nsuby
	AIC(i)=log(covar(i,i))*N(i)+p*v(i);
end;


end
