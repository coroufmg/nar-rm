function ys = msimodel(model,x0,nnt,MY0,MU,ME,it)
% function ys = msimodel(model,x0,nt,MY0,MU,ME) returns model predicted
%       output for the subsystems (model,x0,nnt)
%
% On entry
%	model
%	x0	- coefficients
%	nnt	- number of terms of each subsystem model
%	MY0	- matrix of initial conditions
%	MU	- matrix of input signals
%	ME	- matrix of noise signals
%
% On return
%	ys	- matrix of output signals

% Eduardo Mendes - 3/09/94
% ACSE - Sheffield

% Eduardo Saraiva - 16/02/98
% CPDEE - UFMG 

%Rafael Santos - 18/06/2015
% PPGEE - UFMG

if ((nargin < 3) | (nargin > 7))
	error('msimodel requires 3, 4, 5, 6 or 7 input arguments.');
elseif nargin == 3
	MY0=[];
	MU=[];        
	ME=[];
elseif nargin == 4
	MU=[];
	ME=[];
elseif nargin == 5
	ME=[];
end;

if nargout > 1
	error('msimodel requires 1 output argument at most.');
end;

nsuby=size(nnt,2);

[a,degree]=size(model);

b=size(x0);

if (b ~= sum(nnt))
	error('model is incompatible (nsuby).');
end;

if (a ~= b)
	error('model and coefficients are incompatible (rows).');
end;

[npr,nno,lag,ny,nu,ne]=mget_inf(model,nnt);



% Input Vector

[n,nuu]=size(MU);

if (nuu == 0)
	if nu ~= 0
		error('No input vector.');
	end;

	n=it; % simulation with 291 points. This value must agree with
	       % the lenght of MY vectors.

else
	if n <= lag
		error(sprintf('Number of data points should be greater than %g',lag));
	end;
end;


% Initial Conditions

if isempty(MY0)
	MY0=zeros(lag,nsuby);
end;

[p,q]=size(MY0);

if p ~= lag
	error(sprintf('MY0 is a (%g X %g) matrix.',lag,ny));
end;

% Noise

if isempty(ME)
	ME=zeros(n,nsuby);
else
	[a,b]=size(ME);
	if (b < ne)
		error(sprintf('Number of columns of e should be %g',ne));
	end;
end;

% Calculations
%Termos
%sen y 4000  cos y 5000
%sen u 6000  cos u 7000

y=zeros(n,nsuby);
y(1:lag,:)=MY0(1:lag,:);

pos =ones(1,nsuby);
if nsuby > 1
   for i=2:nsuby
      pos(1,i) = pos(1,i-1) + npr(i-1) + nno(i-1);
   end;
end;

for nn=lag+1:n
    for k=1:nsuby
        for i=pos(k):( pos(k) + npr(k) + nno(k) - 1)
            sinter1=x0(i,1);
            for j=1:degree
                a=model(i,j);
                if ((a == 0) | (a == (-1))), break; end;
                kk=floor(a/1000);
                kk1=floor((a-1000*kk)/100);
                kk2=a-1000*kk-100*kk1;
                if kk == 1
                    sinter1=sinter1*y(nn-kk2,kk1);            
                elseif kk == 2
                    sinter1=sinter1*MU(nn-kk2,kk1);
                elseif kk==3
                    sinter1=sinter1*ME(nn-kk2,kk1);
                elseif kk==4
                    sinter1=sinter1*sin(y(nn-kk2,kk1));
                elseif kk==5
                    sinter1=sinter1*cos(y(nn-kk2,kk1));
                elseif kk==6
                    sinter1=sinter1*sin(MU(nn-kk2,kk1));
                elseif kk==7
                    sinter1=sinter1*cos(MU(nn-kk2,kk1));
                end;
            end;
            if (a == (-1)), break; end;
            y(nn,k)=y(nn,k)+sinter1;
        end;
    end;
end;

ys=y;
