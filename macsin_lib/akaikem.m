function [f, mint] = akaikem(modelo,d,sy)
%
%AKAIKE Calcula atrav�s do crit�rio de informa��o de Akaike o n�mero 
% aproximado de termos de processo a ser considerado na sele��o
% de estrutura de modelos polinomiais.
%
% Entradas:  
%
%          modelo => matriz c�digo dos termos candidatos;
%               d => dados de entrada do sistema a ser modelado;
%               sy => 0 - indica que todos os sistemas ter�o a mesma quantidade de termos;
%                     1 - indica que cada sistema ter� a sua pr�pria quantidade de termos;
%
% Sa�das 
%
%              f => seq��ncia contendo os dados do crit�rio de Akaike;
%           mint => o n�mero de termos de processo a ser considerado.
%
%
%
%Exemplo: 
%       
%dadexe.m
%[f,mint] = akaike(modelo,ui,yi);
%
% M�rcio Barroso
% PPGEE - CPDEE - UFMG
% GRUPO MACSIN
% Belo Horizonte, 25 de maio de 2001
%

%Rafael Santos 18/06/2015
% PPGEE - UFMG

%Define o tamanho do modelo
%------------------------------------------------
np = size(modelo,1);
%------------------------------------------------

%Itera��es para o c�lculo do crit�rio
%------------------------------------------------
if(np>50) %n�mero m�ximo de termos
    np=50;
end;

f=[];
for i =  1 : np
    
    %clc
    disp(sprintf('iteration (%i) from a total of (%i)',i,np))
    %[m,x,e] = orthreg(modelo,u,y,i);
    
    [m,x,ME,va]=morthreg(modelo,0,[],d,i,10);
    %f(i) = aic(i,e,2);
    v=[];
    covar=cov(ME);
%     covar=eye(size(d(1).MY,2));
     N=[];
     for j=1:size(d(1).MY,2)
         v=[v i];
%         covar(j,j)=va(j);
%         %N=[N size(ME,1)/size(d(1).MY,2)];
         N=[N size(ME,1)];
     end;
    AIC = aicm(covar,v,N);
    
    if(sy==0)
      f(i)=min(AIC);
    else
    f=[f;AIC];
    end;
end;
%------------------------------------------------

%Define o n�mero de termos de processo a ser considerado
%------------------------------------------------
if(sy==0)
    mint = find(f == min(f));
else
    for j=1:sy
        mint(j)=find(f(:,j) == min(f(:,j)));
    end;
end;
%------------------------------------------------
%grafico
% figure(1);
% subplot(1,2,1);
% hold on;
% plot(f(:,1));
% plot(mint(1),f(mint(1),1) ,'ro','MarkerSize',10,'MarkerFaceColor','r');
% xlabel(['$n_\theta$ -- N$^o$ de regressores'],'interpreter','latex','fontsize',15);
% %ylabel('${\rm AIC}(n_\theta)$','interpreter','latex','fontsize',15);
% ylabel('${\rm AIC}(n_\theta)=N ~ {\rm ln}({\rm var}\{\xi(k)\})+2n_\theta$','interpreter','latex','fontsize',15);
% title('Polin\^omio $x$','interpreter','latex','fontsize',15);
% hold off;
% 
% subplot(1,2,2);
% hold on;
% plot(f(:,2));
% plot(mint(2),f(mint(2),2) ,'ro','MarkerSize',10,'MarkerFaceColor','r');
% xlabel(['$n_\theta$ -- N$^o$ de regressores'],'interpreter','latex','fontsize',15);
% %ylabel('${\rm AIC}(n_\theta)$','interpreter','latex','fontsize',15);
% ylabel('${\rm AIC}(n_\theta)=N ~ {\rm ln}({\rm var}\{\xi(k)\})+2n_\theta$','interpreter','latex','fontsize',15);
% title('Polin\^omio $y$','interpreter','latex','fontsize',15);
% hold off;



%----------------------FIM-----------------------