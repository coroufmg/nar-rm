function [m,x,ME,va] = morthreg(model,sy,MU,DY,values,N)
% function [m,x,ME,va] = morthreg (model,sy,MU,MY,values,N) returns
%                        model and its coefficients
%
% On entry
%	model	- code for representing a model
% 	sy	- subsystem which model will be derived
%	MU	- matrix of input signals
%	MY	- matrix of output signals
%	values	- [(Number of Process Terms) (Number of Noise terms)]
%	N	- number of noise iterations
%
% On return
%	m	- identified model
%	x	- [(coeff.) (err) (std)]
%	ME	- matriz of residuals
%	va	- variance
%

% Eduardo Mendes - 11/08/94
% ACSE - Sheffield

% Eduardo Saraiva - 16/02/97
% CPDEE - UFMG 

if ((nargin < 5) | (nargin > 6))
	error('morthreg requires 5 or 6 input arguments.');
elseif nargin == 5
	N=0;
end;

[a,b]=size(values);

if ((a*b) > 2)
	error('values is a 2-length vector.');
elseif ((a*b) == 1)
	values=[values 0];
	N=0;
end;

% Process Terms

[npr,nno,lag,ny,nu,ne,model]=mget_inf(model);

[p,q]=size(model);

if values(1) > npr
	values(1)=npr;
end;

if values(2) > nno
	values(2)=nno;
end;

if values(2) == 0
	N=0;
end;

% disp(sprintf('Starting the orthogonalizing procedures for process terms:'));
% disp(sprintf('* building the regressor matrix ...'));

dem=size(DY,2);
Pp=mbuild_p(model,DY(1).MY,MU);
MY=DY(1).MY;
tam=size(DY(1).MY,1); 
for i=2:dem
    auxMY=DY(i).MY;
    MY=[MY; auxMY((lag+1):tam,:)];
    aux=mbuild_p(model,auxMY,MU);
    Pp=[Pp ; aux((lag+1):tam,:)];
end;
    
[n,m]=size(MY);

if sy == 0
   r=1;
   s=m;
   ME=zeros(n,m);
   MSQY=zeros(1,m);
   temp1=zeros(m*(values(1)+values(2)),3);
   temp2=zeros(m,values(1)+values(2));
   MXLS=zeros(m*values(1),3);
   MPIV=zeros(m,values(1));
   MPIVN=zeros(m,values(2));
   MVAR=zeros(1,m);
   MB=zeros(m*(n-lag),values(1));
   MMY=zeros(m*(n-lag),m);
   MAA=zeros(npr,m);
   MBB=zeros(npr,m);
   MPp=zeros(m*n,values(1));
   MPn=zeros(m*n,nno);
   MPn1=zeros(m*(n-lag),nno);
   m=zeros(m*(values(1)+values(2)),q);
else
   r=sy;
   s=sy;
   ME=zeros(n,1);
   MSQY=zeros(1,1);
   temp1=zeros(values(1)+values(2),3);
   temp2=zeros(1,values(1)+values(2));
   MXLS=zeros(values(1),3);
   MPIV=zeros(1,values(1));
   MPIVN=zeros(1,values(2));
   MVAR=zeros(1,1);
   MB=zeros((n-lag),values(1));
   MMY=zeros((n-lag),1);
   MAA=zeros(npr,1);
   MBB=zeros(npr,1);
   MPp=zeros(n,values(1));
   MPn=zeros(n,nno);
   MPn1=zeros((n-lag),nno);
   m=zeros(values(1)+values(2),q);
end;



j=1;

for t=r:s
    MSQY(:,j)=MY((lag+1):n,t)'* MY((lag+1):n,t);
    
%     disp(sprintf('* estimating the parameters of subsystem %d ...',t));
    
    
    [MXLS((j-1)*values(1)+1:j*values(1),:),MPIV(j,:),MVAR(1,j),MB((j-1)*(n-lag)+1:j*(n-lag),:),MMY((j-1)*(n-lag)+1:j*(n-lag),j),MAA((j-1)*npr+1:j*npr,j),MBB((j-1)*npr+1:j*npr,j),MCC((j-1)*npr+1:j*npr,j)]=housels(Pp(lag+1:n,:),MY(lag+1:n,t),values(1),MSQY(1,j));
   
    MPIVP=MPIV;
    
    MPp((j-1)*n+1:j*n,:)=Pp(:,MPIV(j,:));
    
    %ME(:,j)=MY(:,j)-MPp((j-1)*n+1:j*n,:)*MXLS((j-1)*values(1)+1:j*values(1),1);
    
    %Erro corrigido no dia 23/11/2016 MY(:,j) foi substituido por MY(:,t)
    ME(:,j)=MY(:,t)-MPp((j-1)*n+1:j*n,:)*MXLS((j-1)*values(1)+1:j*values(1),1);
    
    j=j+1;
end;


%disp(sprintf('Done.\n'));

if values(2) ~= 0
     disp(sprintf('Starting the orthogonalizing procedures for noise terms:'));
    j=1;
    
    for t=r:s
        temp1((j-1)*(values(1)+values(2))+1:j*(values(1)+values(2))-values(2),:)=MXLS((j-1)*values(1)+1:j*values(1),:);
        
        temp2(j,:)=[MPIV(j,:) zeros(1,values(2))];
        
        j=j+1;
    end;
    
    MXLS=temp1;
    MPIV=temp2;
    
    for i=1:N
         disp(sprintf('* (%g)-th iteration: ',i));
        j=1;
        for t=r:s
            
            ME(:,j)=[zeros(lag,1);ME(lag+1:n,j)];
            
%             disp(sprintf('  building the regressor matrix ...'));
            
            MPn((j-1)*n+1:j*n,:)=mbuild_n(model,MU,MY,ME);
            
            MPn1((j-1)*(n-lag)+1:j*(n-lag),:)=apply_qr(MPn((j-1)*n+lag+1:j*n,:),MB((j-1)*(n-lag)+1:j*(n-lag),:));
            
%             disp(sprintf('  estimating the parameters - subsystem %d ...',t));
            
            [MXLS((j-1)*(values(1)+values(2))+1:j*(values(1)+values(2)),:),MPIV(j,:),MVAR(1,j)]=housels([MB((j-1)*(n-lag)+1:j*(n-lag),:) MPn1((j-1)*(n-lag)+1:j*(n-lag),:)],MMY((j-1)*(n-lag)+1:j*(n-lag),j),values,MSQY(1,j),MXLS((j-1)*(values(1)+values(2))+1:j*(values(1)+values(2)),2),MPIV(j,1:values(1)),MAA((j-1)*npr+1:j*npr,j),MBB((j-1)*npr+1:j*npr,j),MCC((j-1)*npr+1:j*npr,j));
            
            MPIVN(j,:)=MPIV(j,values(1)+1:values(1)+values(2))-values(1);
            
            ME(:,j)=MY(:,j)-[MPp((j-1)*n+1:j*n,:) MPn((j-1)*n+1:j*n,MPIVN(j,:))]*MXLS((j-1)*(values(1)+values(2))+1:j*(values(1)+values(2)),1);
            
            j=j+1;
            
        end;
%         disp(sprintf('\n'));
    end;
    disp(sprintf('Finish of orthogonalizing procedures for noise terms.\n'));

end;
 
if N ~= 0
    MPIV=[MPIVP MPIVN+npr];
end;

j=1;
for t=r:s
    m((j-1)*(values(1)+values(2))+1:j*(values(1)+values(2)),:)=model(MPIV(j,:),:);
    
    va=MVAR;
    
    x=MXLS;
    
    ME(:,j)=[zeros(lag,1);ME(lag+1:n,j)];
    
    j=j+1;
end;
