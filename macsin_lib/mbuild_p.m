function P = mbuild_p(model,MY,MU)
% function P = mbuild_process(model,MY,MU) builds the regressor matrix
%	for the process terms
%
% On entry
%	model	- code for the model representation
%	MY	- matrix of output signals
%	MU	- matrix of input signals

%
%
% On return
%	P	- regressor matrix

% Eduardo Mendes - 11/08/94
% ACSE - Sheffield

% Eduardo Saraiva - 16/02/98
% CPDEE - UFMG 

%Rafael Santos - 18/06/2015
% PPGEE - UFMG

if ((nargin < 2) | (nargin > 3))
	error('mbuild_process requires 2 or 3 input arguments.');
elseif nargin == 2
	MU=[];
end;

[npr,nno,lag,ny,nu]=mget_inf(model);

nte=npr+nno;	% Number of terms

[ans,degree]=size(model);

if isempty(MY)
	if MY ~= 0
		error('MY cannot be an empty matrix.');
	end;
	MY=zeros(n,1);	
end;

% Calculations

%Termos
%sen y 4000  cos y 5000
%sen u 6000  cos u 7000

[n,a]=size(MY);
P=ones(n,npr);

for i=1:npr
    kk=floor(model(i,:)/1000); % Signal
    kk0=floor((model(i,:)-kk*1000)/100); % subsystem
    kk1=model(i,:)-kk*1000-kk0*100;	% lags
    r=find(kk == 3);
    if isempty(r)
        j=find(kk == 1);
        if ~isempty(j)
            for k=1:length(j)
                P(:,i)=P(:,i).*(shift_c(MY(:,kk0(j(k))),kk1(j(k))));
            end;
        end;
        
        j=find(kk == 2);
        if ~isempty(j)
            for k=1:length(j)
                P(:,i)=P(:,i).*(shift_c(MU(:,kk0(j(k))),kk1(j(k))));
                
            end;
        end;
        
        j=find(kk == 4);
        if ~isempty(j)
            for k=1:length(j)
                P(:,i)=P(:,i).*sin((shift_c(MY(:,kk0(j(k))),kk1(j(k)))));
            end;
        end;
        
        j=find(kk == 5);
        if ~isempty(j)
            for k=1:length(j)
                P(:,i)=P(:,i).*cos((shift_c(MY(:,kk0(j(k))),kk1(j(k)))));
            end;
        end;
        
        j=find(kk == 6);
        if ~isempty(j)
            for k=1:length(j)
                P(:,i)=P(:,i).*sin(shift_c(MU(:,kk0(j(k))),kk1(j(k))));
                
            end;
        end;
        
        j=find(kk == 7);
        if ~isempty(j)
            for k=1:length(j)
                P(:,i)=P(:,i).*cos(shift_c(MU(:,kk0(j(k))),kk1(j(k))));
                
            end;
        end;
        
    end;
end;
