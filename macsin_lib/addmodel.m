function newmodel = addmodel(model1,model2,nsuby,avoid)
% function newmodel = addmodel(model1,model2,nsuby,avoid) adds model1 to
% model2 and put the result on newmodel
%
% On entry
%     model1
%     model2
%     nsuby - number of subsystems (default = 1)
%     avoid - 'y' avoid the elimination of equal terms
%
% On return
%     newmodel - model1 + model2

% Eduardo Mendes - 09/13/2001
% UT - EE

if ( (nargin < 2) | (nargin > 4) )
  error('addmodel requires 2, 3 or 4 input arguments.');
elseif nargin == 2
  nsuby=1;
  avoid='n';
elseif nargin == 3
  avoid='n';
end;

if isempty(nsuby)
  nsuby=1;
end;

if isempty(avoid)
  avoid='n';
end;

if ~isstr(avoid)
  avoid='n';
end;

[a,b]=size(nsuby);

if ((a*b) ~= 1)
  error('nsuby is a scalar.');
end;

nsuby=floor(abs(max(1,nsuby)));

[nt1,total1]=size(model1);

[nt2,total2]=size(model2);

deg1=total1/nsuby;
deg2=total2/nsuby;

if deg1 ~= floor(total1/nsuby)
  error('there is a problem with nsuby or model1.');
end;

if deg2 ~= floor(total2/nsuby)
  error('there is a problem with nsuby or model2.');
end;

if deg2 > deg1
  m=zeros(nt1,deg2*nsuby);
  for i=1:nsuby
    m(:,(i-1)*deg2+1:(i-1)*deg2+deg1)=model1(:,(i-1)*deg1+1:i* ...
					     deg1);
  end;
  model1=m;
else
  m=zeros(nt2,deg1*nsuby);
  for i=1:nsuby
    m(:,(i-1)*deg1+1:(i-1)*deg1+deg2)=model2(:,(i-1)*deg2+1:i* ...
					     deg2);
  end;
  model2=m;
end;

newmodel=[model1
	  model2];

% if ~strcmp(avoid,'y')
%   newmodel=elimterm(newmodel);
% end;


