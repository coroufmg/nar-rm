NAR-RM version 1.0 issued on 06 November 2017

This packages contains the implementation of NAR-RM proposed in:

 "Learning robot reaching motions by demonstration using nonlinear autoregressive models"

This package was implemented by the members of the Computer Systems and Robotics Lab. (CORO)
from the Department of Electrical Engineering of Federal University of Minas Gerais, Brazil.
CORO is one of the laboratories associated to the Group of Reasearch and Developement of 
Autonomous Vehicles - PDVA, which is currently working in the development of instrumentation,
navigation, and control of autonomous and semi-autonomos ground and aerial vehicles.
CORO is also associated with National Institute of Science and Technology for Cooperative Autonomous Systems - InSAC. 

The program is free for non-commercial academic use. Please contact the author if you are interested in using the software for commercial purposes.
The software must not be modified or distributed without prior permission of the authors. Please acknowledge the authors in any academic publication
that have made use of this code or part of it. Please use the aforementioned paper for reference.


To get latest update of the software please visit
                      https://bitbucket.org/coroufmg/nar-rm

Please send your feedbacks or questions to:
                      reyfow@gmail.com or rsantos@unifei.edu.br

Videos with mobile robot and industrial manipulator experiments available in
                      https://goo.gl/AqMLAH 


This source code include a matlab functions: 'NAR_RM.m' and 3 subdirectories: 'nar-rm_lib', 'macsin_lib', 'benchmark' and 'DataSet'.

nar-rm_lib: auxiliary functions used in the NAR-RM procedure.

macsin_lib: matlab functions to system identification techniques, implemented by MACSIN group from 
            the Department of Electrical Engineering of Federal University of Minas Gerais, Brazil.

benchmark: contains the data and codes used in the execution of the benchmark 
           "Open-source benchmarking for learned reaching motion generation in robotics"
            available in https://www.amarsi-project.eu/benchmark-framework

DataSet: contains 3 motios from library LASA Handwriting Dataset. Available in: https://bitbucket.org/khansari/lasahandwritingdataset